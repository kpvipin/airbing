﻿$(document).ready(function () {
    $('.create_acc_txt').click(function () {

        $('.create_inner').slideToggle(600);


    });
    $(".rb_login").find("input").click(function () {

        if ($(this).val() == "new") {

            $(".existing").hide();
            $(".create_account").show();
            $(".show_hide").show();
        }
        else {

            $(".existing").show();
            $(".create_account").hide();

            if ($("#hf_show_button").val() == "show") {
                $(".show_hide").show();
            }
            else {
                $(".show_hide").hide();
            }
        }

    });


    if ($(".rb_login").find("input:checked").val() == "new") {

        $(".existing").hide();
        $(".create_account").show();
        $(".show_hide").show();
    }
    else {

        $(".existing").show();
        $(".create_account").hide();
        if ($("#hf_show_button").val() == "show") {
            $(".show_hide").show();
        }
        else {
            $(".show_hide").hide();
        }

    }


    $(".click_links").find("a").click(function () {

        if ($("." + $(this).attr("class").replace("_link", "")).css("display") == "none") {
            $(".password").hide();
            $(".forgot_password").hide();

            $("." + $(this).attr("class").replace("_link", "")).show();
        }
        else {
            $(".password").hide();
            $(".forgot_password").hide();

        }

    });

});