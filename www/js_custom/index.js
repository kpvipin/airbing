﻿$(document).ready(function () {

    


    var from = document.referrer;


    //setting the scroll bar for extended weather
    // $("#content-slider").slider({
    //     animate: true,
    //     change: handleSliderChange,
    //     slide: handleSliderSlide
    // });
    //popup about us

    $(".popup1").click(function (e) {


        e.preventDefault();

        $("#lbl_confirm").hide();
        $(".image").hide();
        $("body").scrollTop(0);
        window.scroll(0, 0);



        popup = $(".about_container");

        $.blockUI({
            message: popup,
            css: {

                top: 100 + 'px',
                left: ($(window).width() - popup.width()) / 2+ 'px',
                position: "absolute",
                width: 400 + 'px'


            }

        });


        $('.blockOverlay').attr('title', 'Click to Close').css('cursor', 'pointer').css('cursor', 'hand').click($.unblockUI);
    });


    //popup feedback function




    $(".popup").click(function (e) {


        e.preventDefault();

        $("#lbl_confirm").hide();
        $(".image").hide();
        $("body").scrollTop(0);
        window.scroll(0, 0);



        popup = $(".contacts_container");

        $.blockUI({
            message: popup,
            css: {

                top: 100 + 'px',
                left: ($(window).width() - popup.width()) / 2 + 'px',
                position: "absolute",
                width: 400 + 'px'


            }

        });


        $('.blockOverlay').attr('title', 'Click to Close').css('cursor', 'pointer').css('cursor', 'hand').click($.unblockUI);
    });


    $(".close_btn").click(function () {

        $('.blockOverlay').click();

    });

    $("#submit").click(function () {

        var block = "";


        $(".contacts_inquire_txt").find("span").each(function () {

            if ($(this).css("visibility") == "visible") {
                block = "yes";
            }

        });



        if (block != "yes") {
            $(".image").show();


            var name = $("#name").val();
            var email = $("#email").val();
           
            var phone = $("#phone").val();
          
            var comments = $("#comments").val();



            $.ajax({
                //the method is in a web service ws_flight.asmx
                url: "ws_flight.asmx/submit_contact",
                data: '{ name: "' + name + '", email: "' + email  + '", phone: "' + phone +  '",comments: "' + comments + '" }',
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    $(".image").hide();
                    $("#lbl_confirm").show();
                    $("#name").val(' ');
                    $("#email").val(' ');
                    
                    $("#phone").val(' ');
                    $("#comments").val(' ');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert(textStatus);
                    src.removeClass("wait");
                }
            });
        }
    });


    $("#reset").click(function () {


        $("#lbl_confirm").hide();
        $("#name").val(' ');
        $("#email").val(' ');
       
        $("#phone").val(' ');
        $("#comments").val(' ');

    });

    //hiding th epreloaded image
    $(".wait_preload").hide();

    $(".autocomplete").live("keyup" ,function () {

        var src = $(this);
        if (jQuery.trim(src.val()) == "") {
            $(".wait_preload").hide();
            src.removeClass("wait");
            return;
        }

        if (jQuery.trim(src.val()) != "" ) {
            //to show a waiting gif at the right end of the textbox
            $(this).addClass("wait");



            //autocomplete function for airline/airport codes and names
            src.autocomplete({
                source: function (request, response) {
                    $.ajax({
                        //the method is in a web service ws_flight.asmx
                        url: "ws_flight.asmx/get_codes",
                        //value of the textbox is passed as the search keyword and its title attribute as the type, ie, flight code, flight name etc. It is based on this the results are being displayed.
                        data: '{name: "' + jQuery.trim(src.val()) + '",type: "' + src.attr("title") + '" }',
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            response($.map(data.d, function (item) {

                                //alert(item.code);

                                return {
                                    //name as the prominent data (ie. code when flight code is selected and name when flight name is selected. Same is the case with airports.
                                    label: item.name,
                                    //code as the secondary data (ie. name when flight code is selected and code when flight name is selected. Same is the case with airports.
                                    value: item.code,
                                    option: this

                                }

                            }))

                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            //alert(textStatus);
                            src.removeClass("wait");
                        }
                    });
                },
                delay: 0,
                minLength: 0,
                select: function (event, ui) {
                   // alert(ui.item.value);
                      $('#hf_airlinecodevalue').val(ui.item.value);


                    //checking conditions and populating the respective text fields on selection of an option from the suggestion box. for eg, if flight name is selected flight code also will be populated with the respective data and vice versa.

                    if (src.attr("class").split(" ")[0] == "txt_inner_name") {
                        src.val(ui.item.label);
                        src.parent().prev().find(".txt_inner_code").val(ui.item.value);
                    }
                    else if (src.attr("class").split(" ")[0] == "txt_inner_code") {
                        src.val(ui.item.label);
                        src.parent().next().find(".txt_inner_name").val(ui.item.value);
                    }


                    return false;

                },

                //hiding the waiting gif on opening the suggestions box
                open: function (event, ui) {
                    src.removeClass("wait");
                }

            });

            //to initialise the search on entering of a single char in the text box
            src.autocomplete("search", src.val());

            //to hide the suggestions box when clicked outside it
            $("body").click(function (e) {

                if (e.target !== src) {
                    src.autocomplete("close");

                    if (src.attr("class").split(" ")[0] == "txt_inner_name") {
                        if (src.val() == "") {
                            src.parent().prev().find(".txt_inner_code").val("");
                        }
                    }
                    else if (src.attr("class").split(" ")[0] == "txt_inner_code") {
                        if (src.val() == "") {
                            src.parent().next().find(".txt_inner_name").val("");
                        }
                    }

//                    //getting the information of stops if the page is flight scheduling
//                    if ($.trim($("title").html()) == "Flight Schedule Services") {

//                        //getting the position of the element where the request has come
//                        var parent = src.parents(".additional_stop");
//                        var pos = $(".txt_container").children().index(parent);

//                        //making sure that the request is from an additional stop
//                        if (pos != -1) {

//                            var stop_array = $("#hf_stops").val().split(",");

//                            var x = 0;
//                            //setting $("#hf_stops")'s value to 0 before reconstructing the string
//                            $("#hf_stops").val("");
//                            //looping through the array to find out if it contais the stop information already
//                            $.each(stop_array, function (i, val) {
//                                if (val.split("|")[0] == pos) {
//                                    if (src.val() == "") {
//                                        val = "";
//                                        x = i;
//                                    }
//                                }
//                                //reconstructing the string
//                                if ($("#hf_stops").val() == "") {
//                                    $("#hf_stops").val(val);
//                                }
//                                else if (x == i) {
//                                    $("#hf_stops").val($("#hf_stops").val() + val);
//                                }
//                                else {
//                                    $("#hf_stops").val($("#hf_stops").val() + "," + val);
//                                }

//                            }); //loop end




//                        } //pos != -1

//                        alert($("#hf_stops").val());

//                    } //$.trim($("title").html()) == "Flight Schedule Services"

                }


            });

        }

    });


    //auto complete change function

    $(".autocomplete").change(function () {

        var src = $(this);

        if (src.attr("class").split(" ")[0] == "txt_inner_name") {
            if (src.val() == "") {
                src.parent().prev().find(".txt_inner_code").val("");
            }
        }
        else if (src.attr("class").split(" ")[0] == "txt_inner_code") {
            if (src.val() == "") {
                src.parent().next().find(".txt_inner_name").val("");
            }
        }
       



    });


  
   
    $('.date').datepicker({

        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: "dd-MM-yy"

    }).attr("readOnly", true);



    //disabling the browser autocomplete
    $("input").attr("autocomplete", "off");


    //extended weathershow / hide option
    $(".extended_weather_btn").click(function (e) {

        e.preventDefault();
        var target = $(".extended_weather");

        //checking whether it is already in the visible state
        if (target.width() > 0) {
            target.animate({ width: "0px", paddingLeft: "0px" }, 1000);
            target.css("border", "none");
            $(this).attr("value", "Extended Weather");
            $(this).html("Extended Weather");
        }
        else {

            target.css("border", "1px solid #c3d3e8");
            target.css("paddingLeft", "25px");
            target.animate({ width: "740px" }, 1000);
            $(this).attr("value", "Hide Extended Weather");
            $(this).html("Hide Extended Weather");
        }

    });

    //toggle between day/night whether
    $(".day_night").click(function () {

        if ($(this).attr("src") == "images/night.png") {
            $(".day").slideUp(500);
            $(".night").delay(600).slideDown(500);
            $(this).attr("src", "images/day.png");
            $(this).attr("title", "Click here for day weather report");
            $("#content-scroll").animate({ height: ($(".night").height() + 30) + "px" });
        }
        else {
            $(".night").slideUp(500);
            $(".day").delay(600).slideDown(500);
            $(this).attr("src", "images/night.png");
            $(this).attr("title", "Click here for night weather report");
            $("#content-scroll").animate({ height: ($(".day").height() + 30) + "px" });
        }

    });

    //find by route find by flight toggle function

    $(".mode").click(function () {

    
        var mode = $(this).find("input:radio:checked").val();
        $(".error").hide();

        if (mode == "flight") {
            $.cookie("state", "flight");
            $("#flight").slideDown(500);
            $("#route").slideUp(500);
        }
        else {
            $.cookie("state", "route");
            $("#flight").slideUp(500);
            $("#route").slideDown(500);
        }


    });

    //toggle the enquiry form based on the state
    var state = $("#hf_state").val().split("_");


    //show/hide flight/route enquiry forms
    if (state[0] == "flight") {
        $("#flight").show();
        $("#route").hide();
        //$(".mode").find("input:radio").val("flight");
    }
    else if (state[0] == "route") {
        $("#route").show();
        $("#flight").hide();
        //$(".mode").find("input:radio").val("route");
    }

    if (state[1] == "collapse") {
        $(".sidebox").hide();
        $(".toggle_enquiry").show();
    }
    else {
        $(".sidebox").show();
        $(".toggle_enquiry").hide();
    }


    //toggle enquiry function

    $(".toggle_enquiry").click(function (e) {

        $.cookie("error", "false")
        e.preventDefault();
        $(".error").hide();
        parent.history.back();
        return false;


    });


    //show/hide flight/route enquiry forms
    if ($.cookie("state") == "flight") {
        $("#flight").show();
        $("#route").hide();
        $("input[name='rb_mode'][value='flight']").attr("checked", true);
    }
    else if ($.cookie("state") == "route") {
        $("#route").show();
        $("#flight").hide();
        $("input[name='rb_mode'][value='route']").attr("checked", true);
    }

    if ($.cookie("error") == "false") {
        $(".error").hide();
        $.cookie("error", "true")
    }


    //setting the height of extended weather (day)
    var day_array = [];
    var last;
    $(".day").each(function () {

        last = $(this);

        day_array.push($(this).height());

    });
    $(".day").height(Math.max.apply(null, day_array) + 9);
    $("#content-scroll").height($(".day").height() + 25);
    last.css("border", "none");

    if ($.browser.mozilla) {
        $(".day").height(Math.max.apply(null, night_array) + 9);
    }

});



//slider functions
function handleSliderChange(e, ui) {
    
    var maxScroll = 1000 - $("#content-scroll").width();
    $("#content-scroll").animate({ scrollLeft: ui.value * (maxScroll / 100) }, 1000);
}

function handleSliderSlide(e, ui) {
    var maxScroll = 1000 - $("#content-scroll").width();
    $("#content-scroll").attr({ scrollLeft: ui.value * (maxScroll / 100) });
    
    
    
    
    
    
    
    
    
}
