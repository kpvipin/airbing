﻿$(document).ready(function () {


    //alert($(".info_inner_middle").width());

    //making the checked attribute of the checkboxes false
    $(".additional_stop").find("cb_time").find("input").attr("checked", "false");

    //date & Time picker

    $('.date_time').datetimepicker({

        ampm: true,
        separator: ' @ ',
        addSliderAccess: true,
        sliderAccessArgs: { touchonly: false, step: '1' },
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: "dd-MM-yy"

    }).attr("readOnly", true);


    //find schedule button click

    $(".get_schedule").click(function (e) {



        //preventing the default behaviour of postbaack of the button
        e.preventDefault();

        //function to check whether the return date field is empty when the query is for a round trip
        if ($(".mode").find("input:checked").val() == "round_trip") {
            if ($(".return_time").find("input").val() == "") {
                $(".return_time").find("input").css("background", "#f9c7dd");
                return false;
            }
            else {
                $(".return_time").find("input").css("background", "#ffffff");
            }

        }
        else {
            $(".return_time").find("input").css("background", "#ffffff");
        }

        if ($(".mode").find("input:checked").val() != "multiple_stops") {
            if ($(".arrival_airport_code").val() == "") {
                $(".arrival_airport_code").css("background", "#f9c7dd");
                return false;
            }
            else {
                $(".arrival_airport_code").css("background", "#ffffff");
            }
        }
        else {
            $(".arrival_airport_code").css("background", "#ffffff");
        }



        src_btn = $(this);
        $("#hf_stops").val("");
        $("#hf_hours").val("");
        $("#hf_last_day").val("");




        //        //initialising a global variable for holding the response xml
        //        var str_response;

        //        //getting the input parameters from the user (all the input boxes are given a class "input_source"
        //        $(".input_source").each(function () {

        //            //initialissing variables to supply for the webservice
        //            var str_origin = "";
        //            var str_dest = "";
        //            var str_from = "";
        //            var str_to = "";

        //            //str_origin will hold the dep. airport code which is in the current input source div
        //            str_origin = $(this).find(".txt_inner_code").val();



        //            //str_dest will hold the arrival airport code which is in the immediately following input source div
        //            str_dest = $(this).next().find(".txt_inner_code").val();

        //            //str_from is the date & time of departure in the formay "yyyy-mm-ddThh:mm" and it is in the date field of the current input source div
        //            str_from = $(this).find(".date_time").val();

        //            //calling the function to change the date time value to the format in which the query expects
        //            str_from = format_date(str_from);

        //            //getting the return date and time if the current div is the last one
        //            if ($(this).attr("class").split(" ")[0] == "arrival_txt") {
        //                //getting the return date & time
        //                if ($("#txt_byroute_date_return").val() != "") {
        //                    str_to = format_date($("#txt_byroute_date_return").val());
        //                }
        //            }


        //            //through an ajax call get the retutrn xml from the departure to arrival airort
        //            $.ajax({
        //                //the method is in a web service ws_flight.asmx
        //                url: "ws_flight.asmx/get_schedule",
        //                data: '{ origin: "' + str_origin + '", dest: "' + str_dest + '", from: "' + str_from + '", to_: "' + str_to + '" }',
        //                dataType: "text/xml",
        //                type: "POST",
        //                contentType: "application/json; charset=utf-8",
        //                dataFilter: function (data) { return data; },
        //                success: function (data) {

        //                    $(".xml").html(data);

        //                    //                    response($.map(data.d, function (item) {
        //                    //                        return {
        //                    //                            //name as the prominent data (ie. code when flight code is selected and name when flight name is selected. Same is the case with airports.
        //                    //                            label: item.name,
        //                    //                            //code as the secondary data (ie. name when flight code is selected and code when flight name is selected. Same is the case with airports.
        //                    //                            value: item.code,
        //                    //                            option: this

        //                    //                        }

        //                    //                    }))
        //                },
        //                error: function (XMLHttpRequest, textStatus, errorThrown) {
        //                    alert(textStatus);
        //                    alert(XMLHttpRequest.responseText);

        //                }
        //            });

        //        });


        //        javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btn_find_flights_byroute", "", true, "byroute", "", false, false))


        $(".txt_container").children().each(function () {


            //getting the position of the element where the request has come
            var parent = $(this);
            var pos = $(".txt_container").children().index(parent);

            //getting the stop information and storing it in a hiddenfield to be accessible by the serverside code
            var src = $(this).find(".txt_inner_code");

            if (src.val() != "") {

                var stop_string = src.val();

                if ($("#hf_stops").val() == "") {
                    $("#hf_stops").val(pos + "|" + stop_string);
                }
                else {
                    $("#hf_stops").val($("#hf_stops").val() + "," + pos + "|" + stop_string);
                } //$("#hf_stops").val() = ""

            } //(src.val() != "")


            //getting the hours in fromation in the hours hiddenfield
            src = $(this).find(".time");

            //getting the position of the hours field in variable
            var pos_time = $(".txt_container").children().index(src.parents(".additional_stop"));

            if (pos_time != -1) {

                if (src.val() != "") {

                    var stop_string = src.val();

                    if ($("#hf_hours").val() == "") {
                        $("#hf_hours").val(pos + "|" + src.val() + ":" + $(this).find(".cb_time").find("input").attr("checked"));
                    }
                    else {
                        $("#hf_hours").val($("#hf_hours").val() + "," + pos + "|" + src.val() + ":" + $(this).find(".cb_time").find("input").attr("checked"));
                    } //$("#hf_stops").val() = ""

                } //(src.val() != "")


            } //if (pos_time != -1)

            //getting the dates in fromation in the hours hiddenfield
            src = $(this).find(".date_time");



            if (src.val() != "") {

                var stop_string = src.val();

                if ($("#hf_last_day").val() == "") {
                    $("#hf_last_day").val(pos + "|" + format_date(src.val()));
                }
                else {
                    $("#hf_last_day").val($("#hf_last_day").val() + "," + pos + "|" + format_date(src.val()));
                } //$("#hf_stops").val() = ""

            } //(src.val() != "")


        });



        $(this).unbind('click').click();

    });
    //find schedule button click end



    //toggle all connection airport links
    $(".all_airports").click(function (e) {

        var target = $(this).parent().find(".connecting_airports");
        if (e.target.className == "list_links all_airports") {
            if (target.css("display") != "none") {
                target.hide();
            }
            else {
                $(".connecting_airports").hide();
                target.show();
            }
        }
    });

    //toggle all connection airline links
    $(".all_airlines").click(function (e) {

        var target = $(this).find(".connecting_airports");

        if (e.target.className == "list_links all_airlines") {
            if (target.css("display") != "none") {
                target.hide();
            }
            else {
                $(".connecting_airports").hide();
                target.show();
            }
        }
    });


    $(".connecting_airports").click(function () {
        $(this).show();
    });

    //close the connections option box
    $(".connection_close").live("click", function () {

        $(this).parent().parent().hide();

    });


    //    //code to show the button add intermediate airport
    //    $(".cb_multiple_stops").click(function () {
    //        if ($(this).find("input").attr("checked") == "checked") {

    //            $(".hide").show();

    //        }
    //        else {
    //            $(".hide").hide();
    //        }
    //    });


    //    //retain the visible state of the button(add intermediate airport) while postback
    //    if ($(".cb_multiple_stops").find("input").attr("checked") == "checked") {

    //        $(".hide").show();

    //    }
    //    else {
    //        $(".hide").hide();
    //    }


    //adding additional stop when clicking add intermediate airport button
    $(".hide").click(function (e) {
        e.preventDefault();
        var clone_ = $(".additional_stop_hidden").clone();

        clone_.removeClass("additional_stop_hidden").addClass("additional_stop").addClass("input_source");
        clone_.find("input").each(function () {

            if ($(this).attr("class") != "find_flights clear_cal") {
                $(this).val("");
            }
        });




        $(".txt_container").find(".input_source").last().before(clone_);

        clone_.find(".stop_no").html("Stop " + $(".txt_container").find(".additional_stop").length);
        clone_.find(".stop_no_2").html("Time to be spent at stop" + $(".txt_container").find(".additional_stop").length);

        clone_.find('.date_time')
                    .removeClass('hasDatepicker')
                    .each(function () {

                        var newName = this.name + ($(".txt_container").find(".additional_stop").length);
                        this.name = newName;
                        this.id = newName;
                    }).datetimepicker({

                        ampm: true,
                        separator: ' @ ',
                        addSliderAccess: true,
                        sliderAccessArgs: { touchonly: false, step: '1' },
                        changeMonth: true,
                        changeYear: true,
                        showButtonPanel: true,
                        dateFormat: "dd-MM-yy"

                    }).attr("readOnly", true);


    });



    //removing the additional stop while clicking close
    $(".close_icon").live("click", function () {
        $(this).parent().remove();


        //adjusting the numbers
        $(".txt_container").find(".additional_stop").each(function () {
            $(this).find(".stop_no").html("Stop " + ($(".txt_container").children().index($(this))));
            $(this).find(".stop_no_2").html("Time to be spent at stop" + ($(".txt_container").children().index($(this))));
        });

    });


    $(".clear_cal").live("click", function (e) {

        e.preventDefault();

        $(this).parent().find(".date_time").val("");

    });

    //one_way round trip toggle function 

    $(".mode").find("input").click(function () {

        if ($(this).val() == "one_way") {

            
            $(".hide").hide();
            $(".arrival_airport").show();
            $(".return_time").hide();
            $(".return_time").find("input").val("");
            $(".additional_stop").remove();

        }
        else if ($(this).val() == "round_trip") {

            $(".return_time").show();
            $(".hide").hide();
            $(".arrival_airport").show();          
            $(".additional_stop").remove();

        }
        else {
            $(".return_time").hide();
            $(".return_time").find("input").val("");
            $(".hide").show();
            $(".arrival_airport").hide();
            $(".arrival_airport").find("input").val("");
        }

    });

    //toggle one way round trip on load

    if ($(".mode").find("input:checked").val() == "one_way") {

        $(".hide").hide();
        $(".arrival_airport").show();
        $(".return_time").hide();
        $(".return_time").find("input").val("");
        $(".additional_stop").remove();

    }
    else if ($(".mode").find("input:checked").val() == "round_trip") {

        $(".return_time").show();
        $(".hide").hide();
        $(".arrival_airport").show();       
        $(".additional_stop").remove();

    }
    else {
        $(".return_time").hide();
        $(".return_time").find("input").val("");
        $(".hide").show();
        $(".arrival_airport").hide();
        $(".arrival_airport").find("input").val("");
    }

   

  //  $('.connection_result_box').equalizeBottoms();





});





//function to change the string date to the required format
function format_date(_date) {
    var time_part = $.trim(_date.split("@")[1]);
    var date_part = $.trim(_date.split("@")[0]);
    var result;

    var day = date_part.split("-")[0];
    var year = date_part.split("-")[2];
    var month = date_part.split("-")[1];

    //get the no. of month
    switch (month) {

    case "January":
        month = '01';
        break;
    case "February":
        month = '02';
        break;
    case "March":
        month = '03';
        break;
    case "April":
        month = '04';
        break;
    case "May":
        month = '05';
        break;
    case "June":
        month = '06';
        break;
    case "July":
        month = '07';
        break;
    case "August":
        month = '08';
        break;
    case "September":
        month = '09';
        break;
    case "October":
        month = '10';
        break;
    case "November":
        month = '11';
        break;
    case "December":
        month = '12';
        break;
}

var time_float;
var hr_part;

//if time is in am
if (time_part.split(" ")[1].toUpperCase() == "AM") {

    time_float = parseFloat(time_part.split(" ")[0].split(":")[0] + "." + time_part.split(" ")[0].split(":")[1]).toFixed(2);


    if (time_float >= 12.00) {

    //changing 12.10 to 00.10
        time_part = (time_float - 12.00).toFixed(2);
    }
    else {
        time_part = time_float;
    }

    time_part = time_part.toString();
    hr_part = (time_part.split(".")[0]).toString();

    //changing 0.10 to 00.10
    if (hr_part.length < 2) {
        hr_part = '0' + hr_part
    }

    time_part = hr_part + ":" + time_part.split(".")[1];

}
else {
    time_float = parseFloat(time_part.split(" ")[0].split(":")[0]) + 12;  

    time_part = time_float.toString() + ":" + time_part.split(" ")[0].split(":")[1]

    
}



date_part = year + "-" + month + "-" + day;

//changing the final return value to 'yyyy-mm-ddThh:mm' format
result = date_part + "T" + time_part;

return result;

}