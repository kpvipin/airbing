﻿<?php

    require_once "comman.php";

    @session_start();

    if(isset($_REQUEST['onlyfindflights']) && $_REQUEST['onlyfindflights']=="Find Flights" && isset($_REQUEST['byflight']) && isset($_REQUEST['flightnumber']) && isset($_REQUEST['flightdate']))

    {   

        //echo date('d-F-Y', strtotime(date('d-m-Y')));

        $flightdate = date('Y/m/d', strtotime($_REQUEST['flightdate']));

        $byflight_title = $_REQUEST['byflight'];

        preg_match("/\((.*)\)/", $byflight_title , $match_from);

        $icao = $match_from[1];

        // $url = 'https://api.flightstats.com/flex/schedules/rest/v1/json/flight/'.$icao.'/'.$_REQUEST['flightnumber'].'/departing/'.$_REQUEST['flightdate'].'?appId=2dfe591f&appKey=f5638cdd75d87be1b8214607d90d1f4a';

        // $flightdata = getAPI($url);

       

        $url = 'https://api.flightstats.com/flex/flightstatus/rest/v2/json/flight/status/'.$icao.'/'.$_REQUEST['flightnumber'].'/dep/'.$flightdate.'?appId=2dfe591f&appKey=89c5ae03ef4d2bf5ef2f43a7b2997b1c&utc=false';

        $flightdata = getAPI($url);

        $_SESSION['fromiata1'] = $flightdata->flightStatuses[0]->departureAirportFsCode;

        $_SESSION['toiata1'] = $flightdata->flightStatuses[0]->arrivalAirportFsCode;

        //echo "<pre>";print_r($_SESSION);die();

        //echo "<pre>";print_r($flightdata);die();

    }

    if(isset($_REQUEST['findflights']))

    {   

        $_SESSION['fromiata'] = $_SESSION['toiata'] ='';

        

        $routedate = date('Y/m/d', strtotime($_REQUEST['routedate']));

        $byroute_title = $_REQUEST['byroute'];

        preg_match("/\((.*)\)/", $byroute_title , $match_from);

        $from = $match_from[1];

        $arrivalroute_title = $_REQUEST['byarrivalroute'];

        preg_match("/\((.*)\)/", $arrivalroute_title , $match_to);

        $to = $match_to[1];

        $_SESSION['fromiata'] = $from;

        $_SESSION['toiata'] = $to;

        $url = 'https://api.flightstats.com/flex/flightstatus/rest/v2/json/route/status/'.$from.'/'.$to.'/dep/'.$routedate.'?appId=2dfe591f&appKey=f5638cdd75d87be1b8214607d90d1f4a&utc=false';

        $data = getAPI($url);

        //echo "<pre>";print_r($data);die;

    }



?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title>air bing</title>

    <meta http-equiv="cache-control" content="max-age=0" />

    <meta http-equiv="cache-control" content="no-cache" />

    <meta http-equiv="expires" content="0" />

    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />

    <meta http-equiv="pragma" content="no-cache" />

    

    <link type="text/css" href="css/ui-lightness/jquery-ui-1.8.19.custom.css" rel="stylesheet" />

    <link href="css/main.css" rel="stylesheet" type="text/css" />

    <link href="css/slider.css" rel="stylesheet" type="text/css" />

    <link href="css/style.css" rel="stylesheet" type="text/css" />

    

    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>

    <script type="text/javascript" src="js/jquery-ui-1.8.19.custom.min.js"></script>

    <script src="js/jquery_cookie.js" type="text/javascript"></script>

    <script src="js/blockUI.js" type="text/javascript"></script>

    <script src="js_custom/index.js" type="text/javascript"></script>



    <script type="text/javascript" src="js/jquery.js"></script>

    <script type='text/javascript' src='js/jquery.autocomplete.js'></script>

    <link rel="stylesheet" type="text/css" href="jquery.autocomplete.css" />



    <link rel="stylesheet" type="text/css" href="css/datepickr.css" />



    <script type="text/javascript">

    $(document).ready(function() {

            

        $("#rfv_byflight").hide();

        $("#rfv_flightnumber").hide();

        $("#rfv_byroute").hide();

        $("#rfv_byarrivalroute").hide();

        

        var name = $.ajax({

            url: 'changeStatus.php',

            type: 'POST', 

            dataType: 'html',

            context: document.body,

            global: false,

            async:false,

            success: function(msg) {

                return msg;

            }

        }).responseText;

        if(name=="flight")

        {   

            $(".route").hide();

            $(".flight").show();

            document.getElementById('route').checked=false;

            document.getElementById('flight').checked=true;

        }   

        else

        {   

            $(".flight").hide();

            $(".route").show();

            document.getElementById('flight').checked=false;

            document.getElementById('route').checked=true;

        }



        $("#byflight").autocomplete("flightdata.php", {

            width: 440,

            matchContains: true,

            //mustMatch: true,

            //minChars: 0,

            //multiple: true,

            //highlight: false,

            //multipleSeparator: ",",

            selectFirst: false

        });

        $("#flight").result(function(event, data, formatted) {

            $("#flight_val").val(data[1]);

        });

        

        $("#byroute").autocomplete("routedata.php", {

            width: 440,

            matchContains: false,

            //mustMatch: true,

            //minChars: 0,

            //multiple: true,

            //highlight: false,

            //multipleSeparator: ",",

            selectFirst: false

        });

        $("#byroute").result(function(event, data, formatted) {

            $("#route_val").val(data[1]);

        });

        $("#byarrivalroute").autocomplete("routedata.php", {

            width: 440,

            matchContains: true,

            //mustMatch: true,

            //minChars: 0,

            //multiple: true,

            //highlight: false,

            //multipleSeparator: ",",

            selectFirst: false

        });

        $("#byarrivalroute").result(function(event, data, formatted) {

            $("#arrivalroute_val").val(data[1]);

        });

        $("#airline").autocomplete("flightdata.php", {

            width: 260,

            matchContains: true,

            //mustMatch: true,

            //minChars: 0,

            //multiple: true,

            //highlight: false,

            //multipleSeparator: ",",

            selectFirst: false

        });

        $('input[type="radio"]').click(function(){

            if($(this).attr("value")=="flight"){

                $.ajax({

                    data: 'name=flight',

                    url: 'changeStatus.php',

                    method: 'POST',

                    success: function(msg) {

                    }

                });

                $(".route").hide();

                $(".flight").show(); 

                           document.getElementById('route').checked=false;

            }

            if($(this).attr("value")=="route"){

                $.ajax({

                    data: 'name=route',

                    url: 'changeStatus.php',

                    method: 'POST',

                    success: function(msg) {

                    }

                });

                

                $(".route").show();

                $(".flight").hide();

                

                document.getElementById('flight').checked=false;

            }

        });

    });

    </script>

    <script type="text/javascript">

    <!--

    // Form validation code will come here.

    function validateFlight()

    {

        //flightform

       if( document.flightform.byflight.value == "" && document.flightform.flightnumber.value == "")

       {

            $("#rfv_byflight").show();

            $("#rfv_flightnumber").show();

            document.flightform.byflight.focus() ;

            return false;

       }

       if( document.flightform.byflight.value == "" )

       {

            $("#rfv_byflight").show();

            document.flightform.byflight.focus() ;

            return false;

       }

       else

            $("#rfv_byflight").hide();

       if( document.flightform.flightnumber.value == "" )

       {

            $("#rfv_flightnumber").show();

            document.flightform.flightnumber.focus() ;

            return false;

       }

       else

            $("#rfv_flightnumber").hide();

       return( true );

    }

    function validateRought()

    {   

        //roughtform

       if( document.roughtform.byroute.value == "" && document.roughtform.byarrivalroute.value == "")

       {

            $("#rfv_byroute").show();

            $("#rfv_byarrivalroute").show();

            document.roughtform.byroute.focus() ;

            return false;

       }

       if( document.roughtform.byroute.value == "" )

       {

            $("#rfv_byroute").show();

            document.roughtform.byroute.focus() ;

            return false;

       }

       else

            $("#rfv_byroute").hide();

       if( document.roughtform.byarrivalroute.value == "" )

       {

            $("#rfv_byarrivalroute").show();

            document.roughtform.byarrivalroute.focus() ;

            return false;

       }

       else

            $("#rfv_byarrivalroute").hide();

       return( true );

    }

    //-->

    </script>



    <style type="text/css">

        .flight_running

        {

            width: 783px;

            height: 48px;

            background: url(images/grey.png) left center no-repeat;

            position: relative;

            top: 260px;

        }



        .flight_running_track

        {

            width: 100%;

            height: 48px;

            background: url(images/tarck.png) right center no-repeat;

            position: absolute;

            /*-webkit-border-radius: 30%;

            -moz-border-radius: 30%;

            border-radius: 30%;*/

        }

    </style>

</head>

<body id="body_id">

    <!-- Start Header -->

    <div id="air_bin_header">

        <div id="main_logo">

            <img alt="" src="images/logo.png" width="211" height="70" /></div>

        <div id="air_bin_menu">

            <ul>

                <li><a title="About Airbing" style="cursor:pointer;" href="about.php"><img src="images/about.png"   />&nbsp;</a></li>

                <li><a title="Give Your Feedback" style="cursor:pointer;" href="feedback.php"><img src="images/feedback.png"  />&nbsp;</a></li>

                <li><a title="Home" style="cursor:pointer;" href="index.html"><img src="images/home.png"  />&nbsp;</a></li>

            </ul>

           <!--  <div id="PanelVoice">

                <br />

                <div id="voice_button" >

                    <a href="yourposts.php">

                        <img src="images/button_count.jpg"  style ="margin-top: 10px;border-top-left-radius: 4px;border-top-right-radius: 4px;border-bottom-right-radius: 4px;border-bottom-left-radius: 4px;"  /></a>

                </div>

            </div> -->

        </div>

    </div>

    <!-- End Header -->

    <div id="air_bin_contant_area_mainx">

        <div id="air_bin_contant_area">

            <div id="air_bin_form">

                <div id="wapper">

                    <center>

                        <div class="container">

                            <!--HEADER -->

                            <div class="wait_preload" style="display: none;">

                                <img src="images/wait.gif" />

                            </div>

                            <!--HEADER END-->



                            <!--container-->

                            <div class="contant_container" style="margin-left: -460;">

                                <!--container_index-->

                                <div class="container_index">

                                    <!--side box-->

                                    <div class="sidebox" style="clear: both;">

                                    <?php if((!isset($_REQUEST['findflights']) && !isset($_REQUEST['onlyfindflights'])) && !isset($_REQUEST['detail'])) { ?>

                                        <div class="side_flight_box">

                                            <div class="byflight" id="radio">

                                                <div class="radio_button">

                                                    <table id="rb_mode" class="mode mode_index" border="0">

                                                        <tr>

                                                            <td>

                                                                <input id="flight" type="radio" name="flight" value="flight" />

                                                                <label for="rb_mode_0" style="color: #fff;">By Flight</label>

                                                            </td>

                                                            <td>

                                                                <input id="route" type="radio" name="route" value="route" checked="checked" />

                                                                <label for="rb_mode_1" style="color: #fff;">By Route</label>

                                                            </td>

                                                        </tr>

                                                    </table>

                                                </div>

                                            </div>

                                        </div>



                                        <!--By Flight-->

                                        <div class="flight" id="flight">

                                            <div class="txt_box">

                                                <form action="index.php" name="flightform" autocomplete="off" method="post" onsubmit="return(validateFlight());">

                                                    <div class="txt_box">

                                                        <div class="txt_name">Flight</div>

                                                        <div class="txt_txtbox">

                                                             <div style="float: left; width: 100%;">

                                                                <input type="text" name="byflight" id="byflight" value="<?php if(isset($_REQUEST['byflight'])) echo $_REQUEST['byflight'];?>" class="txt_inner_name" />

                                                                <input type="hidden" name="flight_val" id="flight_val" value="" />

                                                                <span id="rfv_byflight" style="color: Red; display: inline; position: absolute;">*</span>

                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="txt_box">

                                                        <div class="txt_name">Flight Number</div>

                                                        <div class="txt_txtbox">

                                                            <div style="float: left; width: 100%;">

                                                                <input type="text" name="flightnumber" id="flightnumber" value="<?php if(isset($_REQUEST['flightnumber'])) echo $_REQUEST['flightnumber'];?>" class="txt_inner_name" />

                                                                <span id="rfv_flightnumber" style="color: Red; display: inline;position: absolute;">*</span>

                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="txt_box">

                                                        <div class="txt_name">Date</div>

                                                        <div class="txt_txtbox">

                                                            <!-- <input type="text" name="flightdate" id="flightdate" value="<?php if(isset($_REQUEST['flightdate'])) echo $_REQUEST['flightdate'];?>" class="txt_inner_name" /> -->

                                                             <input type="text" name="flightdate" id="flightdate" value="<?php echo date('d-F-Y');?>" class="txt_inner_name" />

                                                        </div>

                                                    </div>

                                                    <div class="txt_box">

                                                        <div class="txt_name">

                                                        </div>

                                                        <div class="txt_txtbox">

                                                            <input type="submit" name="onlyfindflights" value="Find Flights" id="onlyfindflights" class="find_flights" />

                                                        </div>

                                                    </div>

                                                </form>

                                            </div>

                                        </div>

                                        <!--By Flight End-->

                                        <!--By Route-->

                                        <div class="route" id="route">

                                             <form action="index.php" name="roughtform" method="post" autocomplete="off" onsubmit="return(validateRought());">

                                                <div class="txt_box" style="margin-top: 10px;">

                                                    <div class="txt_name">Dep. Airport</div>

                                                    <div class="txt_txtbox" style="margin-left: 0px;">

                                                        <div style="float: left; width: 100%;">

                                                           <input type="text" name="byroute" id="byroute" value="<?php if(isset($_REQUEST['byroute'])) echo $_REQUEST['byroute'];?>" class="txt_inner_name" />

                                                            <input type="hidden" name="route_val" id="route_val" value="" />

                                                            <span id="rfv_byroute" style="color: Red; display: inline;position: absolute;">*</span>

                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="txt_box">

                                                    <div class="txt_name" style="width: 100px;">Arrival Airport </div>

                                                    <div class="txt_txtbox" style="margin-left: 8px;">

                                                         <div style="float: left; width: 100%;">

                                                            <input type="text" name="byarrivalroute" id="byarrivalroute" value="<?php if(isset($_REQUEST['byarrivalroute'])) echo $_REQUEST['byarrivalroute'];?>" class="txt_inner_name" />

                                                            <input type="hidden" name="arrivalroute_val" id="arrivalroute_val" value="" />

                                                            <span id="rfv_byarrivalroute" style="color: Red; display: inline;position: absolute;">*</span>

                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="txt_box">

                                                    <div class="txt_name">Date</div>

                                                    <div class="txt_txtbox">

                                                        <!-- <input type="text" name="routedate" id="routedate" value="<?php if(isset($_REQUEST['routedate'])) echo $_REQUEST['routedate'];?>" class="txt_inner_name" /> --> 

                                                        <input type="text" name="routedate" id="routedate" value="<?php echo date('d-F-Y');?>" class="txt_inner_name" /> 

                                                        

                                                    </div>

                                                </div>

                                                <div class="txt_box">

                                                    <div class="txt_name">Airline</div>

                                                    <div class="txt_txtbox" style="margin-left: 0px;">

                                                        <div>

                                                            <input type="text" name="airline" id="airline" placeholder="Optional" value="<?php if(isset($_REQUEST['airline'])) echo $_REQUEST['airline'];?>" class="txt_inner_name" />

                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="txt_box">

                                                    <div class="txt_name"></div>

                                                    <div class="txt_txtbox" style="margin-left: 0px;">

                                                        <input type="submit" name="findflights" value="Find Flights" id="findflights" class="find_flights" />

                                                    </div>

                                                </div>

                                            </form>

                                        </div>

                                        <!--By Route End-->

                                        <?php } ?>

                                        <div id="error_info" style="text-align: justify; padding: 10px; margin-top: 10px; float: left;">

                                            <span id="lbl_error" class="error" style="font-size:11pt;"></span>

                                        </div>

                                    </div>

                                    <!--side box end-->

                                    <div id="panel_image"></div>



                                    <!-- Start Route list -->

                                    <?php 

                                    if((isset($_REQUEST['findflights']) && $_SESSION['status']=="route") && (empty($_REQUEST['airline'])))

                                    { ?>

                                    <div class="panel_route">
                                        
                                        <div class="container_byflight" style="margin-top: 10px;margin-bottom: 64px;">
                                            <span style="font-size: 16px;float:left;font-weight:bold;">
                                                <?php
                                                $departureAirportName = getRow('tbl_airport',null,"`iata` LIKE '".$data->request->departureAirport->fsCode."'"); 
                                                $arrivalAirportName = getRow('tbl_airport',null,"`iata` LIKE '".$data->request->arrivalAirport->fsCode."'"); 
                                                ?>
                                                <?php echo $data->request->departureAirport->fsCode.' - '.$departureAirportName['airport_name']; ?> <img src='images/air2.png' height='16' />                                                
                                                <?php echo $data->request->arrivalAirport->fsCode.' - '.$arrivalAirportName['airport_name']; ?>
                                                <br />
                                                
                                            </span> 
                                            <br/>
                                            <br/><br/>
                                            <br/>
                                            <span style="font-size: 14px;float:left;font-weight:bold;">
                                               <?php echo date("F d,Y",strtotime($routedate)); ?>
                                            </span> 
                                            
                                            <div style="margin-bottom: -4em;float: right; /* background: url(../images/menu1.jpg);*/">

                                                <form action="index.php" method="post">

                                                    <div style="float: right; margin-top: 11px">

                                                        <input id="backdetail" class="find_flights toggle_enquiry" type="submit" value="<< Back " name="backdetail" style="display: inline;">

                                                    </div>

                                                    <input type="hidden" name="byroute" id="byroute" value="<?php echo $_REQUEST['byroute'];?>" />

                                                    <input type="hidden" name="byarrivalroute" id="byarrivalroute" value="<?php echo $_REQUEST['byarrivalroute'];?>" />

                                                    <input type="hidden" name="routedate" id="routedate" value="<?php echo $_REQUEST['routedate'];?>" />

                                                </form>

                                                </br></br></br>

                                                <span id="lbl_enq_flight" class="flight_info" style="color:White;font-family:Arial;font-size:12pt;font-weight:bold;"><?php 

                                                $count = 0;

                                                foreach ($data->flightStatuses as $key => $flight_value) { 

                                                    $flightType = $flight_value->schedule->flightType;

                                                    if($flightType=='J' || $flightType=='S' || $flightType=='U')

                                                    {

                                                        $count ++;

                                                    }

                                                }

                                                echo $count." Flights found for your request";

                                                //if(isset($data->flightStatuses)) echo count($data->flightStatuses)." Flights found for your request";?></span>



                                            </div>

                                            <?php if(count($data->flightStatuses)!=0) { ?>

                                            <div class="byflight_box" style="width: 101%; height: 25px;">

                                                <div class="byroute_departure" style="width: 5%; border-left: none;">Code</div>

                                                <div class="byroute_departure" style="width: 25%">Airline</div>

                                                <div class="byroute_departure" style="width: 10%">Flight Number</div>

                                                <div class="byroute_departure" style="width: 15%">Deparure</div>

                                                <div class="byroute_departure" style="width: 15%">Arrival</div>

                                                <div class="byroute_departure" style="width: 15%; border-right: none;">Status</div>

                                                <div class="byroute_departure" style="width: 12%; border: none;"></div>

                                            </div>



                                            <?php } if(!empty($data->flightStatuses)) { $flightStatuses = $data->flightStatuses; 
                                               $X=0; 
                                            foreach ($flightStatuses as $key => $flight_value) { 
                                                $codeshareicon ='';
                                                if($flight_value->codeshares && count($flight_value->codeshares)>0){
                                                        
                                                    foreach($flight_value->codeshares as $valval){

                                                        //$codeshareAry[] = $flight_value->codeshares;
                                                        if($valval->relationship=='X'){
                                                     
                                                            $flight_value->carrierFsCode = $valval->fsCode;
                                                            $flight_value->flightNumber = $valval->flightNumber;            
                                                        }
                                                    }    
                                                    $codeshareicon = "<img src='images/merge.png' height='16' />";
                                                } 
                                                $X++;
                                                $flightType = $flight_value->schedule->flightType;

                                                if($flightType=='J' || $flightType=='S' || $flightType=='U')

                                                {

                                            ?>

                                            <form action="index.php" method="post">

                                            <div class="airport_box " style="width: 100%;">

                                                <div class="airport_name" style="width: 5%; border-left: none;">

                                                    <span id="rt_flights_ctl01_lbl_flight_code" class="Air India"><?php echo $flight_value->carrierFsCode;?>

                                                    </span>

                                                    <div title="Codeshare" style="float: right; margin-top: 2px; margin-right: 5px;"></div>

                                                </div>

                                                <div class="airport_name" style="width: 25%">

                                                    <?php $airline = getRow('tbl_airlines',null,"`icao` LIKE '".$flight_value->carrierFsCode."'"); 



                                                    if(file_exists("logos/".strtolower($flight_value->carrierFsCode).".png"))

                                                    {   

                                                        $countryimg = '<img src="logos/'.strtolower($flight_value->carrierFsCode).'.png'.'" style="position:relative;top:1px;" onError="imgError(this);" width="27" height="20" />';

                                                    }

                                                    else

                                                        $countryimg = '<img src="images/white.jpg" style="position:relative;top:1px;" onError="imgError(this);" width="27" height="20" />';

                                                    echo $countryimg;



                                                    echo "&nbsp;&nbsp;".$airline['airline_name']; ?>

                                                </div>

                                                <div class="airport_name_center" style="width: 10%;">

                                                    <span id="rt_flights_ctl01_lbl_flight_no">

                                                        <?php echo $flight_value->flightNumber; ?>

                                                    </span>

                                                </div>

                                                <div class="airport_name_2" style="width: 15%">

                                                    <?php $datetime = preg_split("/[T,]+/", $flight_value->departureDate->dateLocal);

                                                    echo date('g:i A, M d',strtotime($datetime[0]." ".$datetime[1])).'&nbsp;&nbsp;&nbsp;'; ?>

                                                </div>

                                                <div class="airport_name_2" style="width: 15%">

                                                    <?php $datetime = preg_split("/[T,]+/", $flight_value->arrivalDate->dateLocal);

                                                        echo date('g:i A, M d',strtotime($datetime[0]." ".$datetime[1])).'&nbsp;&nbsp;&nbsp;'; ?>

                                                </div>

                                                <div class="airport_name_2" style="width: 15%">

                                                    <?php 

                                                    if($flight_value->status=='L')

                                                        echo 'Landed&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

                                                    else if($flight_value->status=='A')  

                                                        echo 'Active&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                                    else if($flight_value->status=='C')  
                                                        echo 'Canceled&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                                    else if($flight_value->status=='U')  

                                                        echo 'Unknown&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

                                                    else 

                                                        echo 'Scheduled&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; 

                                                    if($flight_value->delays->departureGateDelayMinutes > 10) { 

                                                        echo "<span style='width: 70%;background: red;'>    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";

                                                    }

                                                    ?>

                                                </div>

                                                <div class="airport_name_2" style="width: 10%; border-right: none;">

                                                    <input type="hidden" name="byroute" id="byroute" value="<?php echo $_REQUEST['byroute'];?>" />

                                                    <input type="hidden" name="byarrivalroute" id="byarrivalroute" value="<?php echo $_REQUEST['byarrivalroute'];?>" />

                                                    <input type="hidden" name="routedate" id="routedate" value="<?php echo $_REQUEST['routedate'];?>" />

                                                    <input type="hidden" id="flightdataid" name="flightdataid" value="<?php echo $data->flightStatuses[$key]->flightId;?>"  />

                                                    <?php if(isset($data->flightStatuses[$key+1]->delays->departureGateDelayMinutes)) { ?>

                                                     <input type="hidden" id="flightdelays" name="flightdelays" value="<?php echo $data->flightStatuses[$key+1]->delays->departureGateDelayMinutes;?>"  /> 

                                                     <?php } else { ?>

                                                     <input type="hidden" id="flightdelays" name="flightdelays" value="<?php echo '';?>"  />

                                                     <?php }?>

                                                    <input class="find_flights" type="submit" value="Details" name= "detail" />
                                                    <?php echo $codeshareicon; ?>
                                                </div>

                                            </div>

                                             </form>

                                            <?php } } }?>

                                        </div>

                                    </div>

                                    <?php } 

                                    if((isset($_REQUEST['findflights']) && $_SESSION['status']=="route") && (!empty($_REQUEST['airline']))) {  ?>

                                    <div class="panel_route">

                                        <div class="container_byflight" style="margin-top: 10px;margin-bottom: 64px;">

                                            <div style="margin-bottom: -4em;float: right; /* background: url(../images/menu1.jpg);*/">

                                                <form action="index.php" method="post">

                                                    <div style="float: right; margin-top: 11px">

                                                        <input id="backdetail" class="find_flights toggle_enquiry" type="submit" value="<< Back " name="backdetail" style="display: inline;">

                                                    </div>

                                                    <input type="hidden" name="byroute" id="byroute" value="<?php echo $_REQUEST['byroute'];?>" />

                                                    <input type="hidden" name="byarrivalroute" id="byarrivalroute" value="<?php echo $_REQUEST['byarrivalroute'];?>" />

                                                    <input type="hidden" name="routedate" id="routedate" value="<?php echo $_REQUEST['routedate'];?>" />

                                                </form>

                                                </br></br></br>

                                                <span id="lbl_enq_flight" class="flight_info" style="color:White;font-family:Arial;font-size:12pt;font-weight:bold;"><?php 

                                                $count = 0;

                                                foreach ($data->flightStatuses as $key => $flight_value) { 

                                                    $flightType = $flight_value->schedule->flightType;

                                                    $airline_temp = getRow('tbl_airlines',null,"`icao` LIKE '".$flight_value->carrierFsCode."'");

                                                    if(($flightType=='J' || $flightType=='S' || $flightType=='U') && $airline_temp['airline']==$_REQUEST['airline'])

                                                    {

                                                        $count ++;

                                                    }

                                                }

                                                echo $count." Flights found for your request";

                                                //if(isset($data->flightStatuses)) echo count($data->flightStatuses)." Flights found for your request";?></span>



                                            </div>

                                            <?php if(count($data->flightStatuses)!=0) { ?>

                                            <div class="byflight_box" style="width: 101%; height: 25px;">

                                                <div class="byroute_departure" style="width: 5%; border-left: none;">Code</div>

                                                <div class="byroute_departure" style="width: 20%">Airline</div>

                                                <div class="byroute_departure" style="width: 15%">Flight Number</div>

                                                <div class="byroute_departure" style="width: 15%">Deparure</div>

                                                <div class="byroute_departure" style="width: 15%">Arrival</div>

                                                <div class="byroute_departure" style="width: 15%; border-right: none;">Status</div>

                                                <div class="byroute_departure" style="width: 12%; border: none;"></div>

                                            </div>



                                            <?php } if(!empty($data->flightStatuses)) { $flightStatuses = $data->flightStatuses; 

                                            foreach ($flightStatuses as $key => $flight_value) { 

                                                $flightType = $flight_value->schedule->flightType;

                                                $airline = getRow('tbl_airlines',null,"`icao` LIKE '".$flight_value->carrierFsCode."'");

                                                if(($flightType=='J' || $flightType=='S' || $flightType=='U') && ($airline['airline']==$_REQUEST['airline']))

                                                {

                                            ?>

                                            <form action="index.php" method="post">

                                            <div class="airport_box " style="width: 100%;">

                                                <div class="airport_name" style="width: 5%; border-left: none;">

                                                    <span id="rt_flights_ctl01_lbl_flight_code" class="Air India"><?php echo $flight_value->carrierFsCode;?>

                                                    </span>

                                                    <div title="Codeshare" style="float: right; margin-top: 2px; margin-right: 5px;"></div>

                                                </div>

                                                <div class="airport_name" style="width: 20%">

                                                    <?php  

                                                    if(file_exists("logos/".strtolower($flight_value->carrierFsCode).".png"))

                                                    {   

                                                        $countryimg = '<img src="logos/'.strtolower($flight_value->carrierFsCode).'.png'.'" style="position:relative;top:1px;" onError="imgError(this);" width="27" height="20" />';

                                                    }

                                                    else

                                                        $countryimg = '<img src="images/white.jpg" style="position:relative;top:1px;" onError="imgError(this);" width="27" height="20" />';

                                                    echo $countryimg;



                                                    echo "&nbsp;&nbsp;".$airline['airline_name']; ?>

                                                </div>

                                                <div class="airport_name_center" style="width: 15%">

                                                    <span id="rt_flights_ctl01_lbl_flight_no">

                                                        <?php echo $flight_value->flightNumber; ?>

                                                    </span>

                                                </div>

                                                <div class="airport_name_2" style="width: 15%">

                                                    <?php $datetime = preg_split("/[T,]+/", $flight_value->departureDate->dateLocal);

                                                    echo date('g:i A, M d',strtotime($datetime[0]." ".$datetime[1])).'&nbsp;&nbsp;&nbsp;'; ?>

                                                </div>

                                                <div class="airport_name_2" style="width: 15%">

                                                    <?php $datetime = preg_split("/[T,]+/", $flight_value->arrivalDate->dateLocal);

                                                        echo date('g:i A, M d',strtotime($datetime[0]." ".$datetime[1])).'&nbsp;&nbsp;&nbsp;'; ?>

                                                </div>

                                                <div class="airport_name_2" style="width: 15%">

                                                    <?php 

                                                    if($flight_value->status=='L')

                                                        echo 'Landed&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

                                                    else if($flight_value->status=='A')  

                                                        echo 'Active&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

                                                    else if($flight_value->status=='U')  

                                                        echo 'Unknown&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                                    else if($flight_value->status=='C')  

                                                        echo 'Canceled&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

                                                    else 

                                                        echo 'Scheduled&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; 

                                                    if($flight_value->delays->departureGateDelayMinutes > 10) { 

                                                        echo "<span style='width: 70%;background: red;'>    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";

                                                    }

                                                    ?>

                                                </div>

                                                <div class="airport_name_2" style="width: 10%; border-right: none;">

                                                    <input type="hidden" name="byroute" id="byroute" value="<?php echo $_REQUEST['byroute'];?>" />

                                                    <input type="hidden" name="byarrivalroute" id="byarrivalroute" value="<?php echo $_REQUEST['byarrivalroute'];?>" />

                                                    <input type="hidden" name="routedate" id="routedate" value="<?php echo $_REQUEST['routedate'];?>" />

                                                    <input type="hidden" id="flightdataid" name="flightdataid" value="<?php echo $data->flightStatuses[$key]->flightId;?>"  />

                                                    <?php if(isset($data->flightStatuses[$key+1]->delays->departureGateDelayMinutes)) { ?>

                                                     <input type="hidden" id="flightdelays" name="flightdelays" value="<?php echo $data->flightStatuses[$key+1]->delays->departureGateDelayMinutes;?>"  /> 

                                                     <?php } else { ?>

                                                     <input type="hidden" id="flightdelays" name="flightdelays" value="<?php echo '';?>"  />

                                                     <?php }?>

                                                     <input type="hidden" name="airline" id="airline" value="<?php echo $_REQUEST['airline'];?>" />

                                                    <input class="find_flights" type="submit" value="Details" name= "detail" />

                                                </div>

                                            </div>

                                             </form>

                                            <?php } } }?>

                                        </div>

                                    </div>

                                    <?php }

                                    ?>

                                    <!-- End Route List -->

                                </div>

                            </div>



                            <!-- Start Flight Detail -->

                            <?php

                                

                                if(isset($_REQUEST['onlyfindflights']) && $_SESSION['status']=="flight") {

                            ?>

                            <?php 

                                if(count($flightdata->flightStatuses)==0) { ?>

                                <div style="margin-top: -15em; margin-left: -20em;">

                                    <form action="index.php" method="post">

                                        <input type="hidden" name="byflight" id="byflight" value="<?php echo $_REQUEST['byflight'];?>" />

                                        <input type="hidden" name="flightnumber" id="flightnumber" value="<?php echo $_REQUEST['flightnumber'];?>" />

                                        <input type="hidden" name="flightdate" id="flightdate" value="<?php echo $_REQUEST['flightdate'];?>" />

                                        <div style="float: right; margin-top: -10em;margin-right: 15em;">

                                            <input id="backflights" class="find_flights toggle_enquiry" type="submit" value="<< Back " name="backflights" style="display: inline;">

                                        </div>

                                    </form>

                                    <font color="red" size="+1">We are sorry, no real time data is available for this request</font>

                                </div>

                                <?php } else { ?>

                            <div class="contant_container" style="margin-left: -460px;margin-top: -100em;">

                                <div style="float: right; margin-top: -60px;/* background: url(../images/menu1.jpg);*/">

                                    <form action="index.php" method="post">

                                        <input type="hidden" name="byflight" id="byflight" value="<?php echo $_REQUEST['byflight'];?>" />

                                        <input type="hidden" name="flightnumber" id="flightnumber" value="<?php echo $_REQUEST['flightnumber'];?>" />

                                        <input type="hidden" name="flightdate" id="flightdate" value="<?php echo $_REQUEST['flightdate'];?>" />

                                        <div style="float: right; margin-top: -10px">

                                            <input id="backflights" class="find_flights toggle_enquiry" type="submit" value="<< Back " name="backflights" style="display: inline;">

                                        </div>

                                    </form>

                                    <br><br><br>



                                    <span id="lbl_enq_flight" class="flight_info" style="color:White;font-family:Arial;font-size:12pt;font-weight:bold;">

                                        <?php $airline = getRow('tbl_airlines',null,"`icao` LIKE '".$flightdata->request->airline->requestedCode."'"); ?>

                                        <span style="color:Yellow">

                                           <?php echo "(".$flightdata->request->airline->requestedCode.")&nbsp;&nbsp;".$airline['airline_name'];?>

                                        </span>

                                        | Flight Number 

                                        <span style="color:Yellow">

                                            <?php echo $flightdata->request->flight->requested.'&nbsp;&nbsp;('.$flightdata->request->airline->requestedCode.$flightdata->request->flight->requested.')';?>

                                        </span>

                                    </span>

                                </div>

                           

 

                                <div id="panel_flight">

                                    <div class="container_byflight_inner">

                                        <div class="container_byflight_box">

                                            <div class="byflight_box">

                                                <div class="byflight_departure">Departure</div>

                                            </div>

                                            <div class="status">

                                                <div class="byflight_departure">

                                                    <div class="arrival_txt" style="float: right;">

                                                        <span id="lbl_flight_status"><?php if(isset($flightdata->flightStatuses[0]->delays->departureGateDelayMinutes) && $flightdata->flightStatuses[0]->delays->departureGateDelayMinutes > 10) echo 'Takeoff '; ?></span>

                                                        <span id="lbl_late_status" style="color:Yellow"><?php if(isset($flightdata->flightStatuses[0]->delays->departureGateDelayMinutes) && $flightdata->flightStatuses[0]->delays->departureGateDelayMinutes > 10) 

                                                            {   echo $flightdata->flightStatuses[0]->delays->departureGateDelayMinutes.' min Late';

                                                            }

                                                            ?>

                                                        </span>

                                                    </div>

                                                </div>

                                            </div>

                                             

                                            <div class="airport_box">

                                                <div class="airport_name">Airport</div>

                                                <div class="byflight_name" style="padding-top: 2px;">:<?php $airport = getRow('tbl_airport',null,"iata LIKE '".$flightdata->flightStatuses[0]->departureAirportFsCode."'"); echo '&nbsp;'.$airport['airport_name']." ,".$airport['country']."(".$airport['iata'].")"; ?>

                                                </div>

                                            </div>

                                            <div class="scheduledtime_box">

                                                <div class="airport_name">Scheduled Time</div>

                                                <div class="byflight_name" style="padding-top: 2px;">:<?php if(isset($flightdata->flightStatuses[0]->operationalTimes->scheduledGateDeparture->dateLocal)) 

                                                    {   $datetime = preg_split("/[T,]+/", $flightdata->flightStatuses[0]->operationalTimes->scheduledGateDeparture->dateLocal);

                                                        echo "&nbsp;".date('d M Y, h:i A',strtotime($datetime[0]." ".$datetime[1])).'&nbsp;';

                                                    }



                                                    if(isset($flightdata->flightStatuses[0]->delays->departureGateDelayMinutes) && $flightdata->flightStatuses[0]->delays->departureGateDelayMinutes > 10)

                                                    {

                                                        if(isset($flightdata->flightStatuses[0]->operationalTimes->estimatedGateDeparture->dateLocal)) 

                                                        {   $datetime = preg_split("/[T,]+/", $flightdata->flightStatuses[0]->operationalTimes->estimatedGateDeparture->dateLocal);

                                                            echo "&nbsp;"."<span style='font-size:13px;'><img title='Expected Departure Time' height='16' src='images/expected.png' />[".date('d M Y, h:i A',strtotime($datetime[0].' '.$datetime[1])).']<span>';

                                                        }

                                                    }

                                                    ?>

                                                </div>

                                            </div>

                                            <div class="airport_box">

                                                <div class="airport_name">Actual Time</div>

                                                <div class="byflight_name" style="padding-top: 2px;">:

                                                    <?php if(isset($flightdata->flightStatuses[0]->operationalTimes->actualGateDeparture->dateLocal)) 

                                                    {   $datetime = preg_split("/[T,]+/", $flightdata->flightStatuses[0]->operationalTimes->actualGateDeparture->dateLocal);

                                                        echo "&nbsp;".date('d M Y, h:i A',strtotime($datetime[0]." ".$datetime[1])).'&nbsp;&nbsp;&nbsp;';

                                                    }

                                                    ?>

                                                </div>

                                            </div>

                                            <div class="scheduledtime_box">

                                                <div class="airport_name">Terminal</div>

                                                <div class="byflight_name" style="padding-top: 2px;">:<?php if(isset($flightdata->flightStatuses[0]->airportResources->departureTerminal)) echo "&nbsp;".'Terminal '.$flightdata->flightStatuses[0]->airportResources->departureTerminal;

                                                    if(isset($flightdata->flightStatuses[0]->airportResources->departureGate))

                                                        echo ' - Gate '.$flightdata->flightStatuses[0]->airportResources->departureGate;

                                                ?>

                                                </div>

                                            </div>

                                            <div class="airport_box">

                                                <div class="airport_name">Local Time</div>

                                                <div class="byflight_name" style="padding-top: 2px;">:

                                                    <?php                                                     

                                                    foreach ($flightdata->appendix->airports as $value) {

                                                         

                                                        if($value->iata==$_SESSION['fromiata1']) 

                                                        {

                                                            if(isset($value->localTime)) 

                                                            {   $datetime = preg_split("/[T,]+/", $value->localTime);

                                                                echo "&nbsp;".date('d M Y, h:i A',strtotime($datetime[0]." ".$datetime[1])).'&nbsp;&nbsp;&nbsp;';

                                                            }

                                                        }                                                       

                                                    }                                                    

                                                    ?>

                                                     

                                                </div>

                                            </div>

                                        </div>

                                        

                                        <div class="container_byflight_box" style="margin-right: 0px; float: right;">

                                            <div class="byflight_box">

                                                <div class="byflight_departure">Arrival</div>

                                            </div>

                                            <div class="status">

                                                <div class="byflight_departure">

                                                <?php if($flightdata->flightStatuses[0]->status=='A') { ?>

                                                    <div class="arrival_txt" style="float: right; margin-right: 0px;">

                                                        </div>

                                                    <div class="arrival_txt" style="float: right; margin-right: 45px;">

                                                        <span id="lbl_flight_status"><?php echo 'Active';?></span></div>

                                                    <div class="delayed_round" style="float: right;">

                                                        <img id="img_flight_status" src="images/round_other.png" style="border-width:0px;"></div>

                                                    <div class="arrival_txt" style="float: right; ">Flight Status:&nbsp;&nbsp;</div>
                                                <?php }elseif($flightdata->flightStatuses[0]->status=='C') { ?>

                                                    <div class="arrival_txt" style="float: right; margin-right: 0px;">

                                                        </div>

                                                    <div class="arrival_txt" style="float: right; margin-right: 45px;">

                                                        <span id="lbl_flight_status"><?php echo 'Canceled';?></span></div>

                                                    <div class="delayed_round" style="float: right;">

                                                        <img id="img_flight_status" src="images/round_cancelled.png" style="border-width:0px;"></div>

                                                    <div class="arrival_txt" style="float: right; ">Flight Status:&nbsp;&nbsp;</div>

                                                <?php } else if($flightdata->flightStatuses[0]->status=='S') { ?>

                                                    <div class="arrival_txt" style="float: right; margin-right: 0px;">

                                                        </div>

                                                    <div class="arrival_txt" style="float: right; margin-right: 45px;">

                                                        <span id="lbl_flight_status"><?php echo 'Scheduled';?></span></div>

                                                    <div class="delayed_round" style="float: right;">

                                                        <img id="img_flight_status" src="images/round_other.png" style="border-width:0px;"></div>

                                                    <div class="arrival_txt" style="float: right; ">Flight Status:&nbsp;&nbsp;</div>

                                                <?php } else { ?>

                                                    <div class="arrival_txt" style="float: right; margin-right: 0px;">

                                                        <span id="lbl_late_status" style="color:Yellow"><?php if(isset($flightdata->flightStatuses[0]->delays->departureGateDelayMinutes)) echo $flightdata->flightStatuses[0]->delays->departureGateDelayMinutes." min Late"; ?> </span></div>

                                                    <div class="arrival_txt" style="float: right; margin-right: 45px;">

                                                        <span id="lbl_flight_status">Landed</span></div>

                                                    <div class="delayed_round" style="float: right;">

                                                        <img id="img_flight_status" src="images/round_landed.png" style="border-width:0px;"></div>

                                                    <div class="arrival_txt" style="float: right; ">Flight Status:&nbsp;&nbsp;</div>

                                                <?php } ?>

                                                </div>

                                            </div>

                                            <div class="airport_box">

                                                <div class="airport_name">Airport</div>

                                                <div class="byflight_name" style="padding-top: 2px;">:<?php $airport = getRow('tbl_airport',null,"iata LIKE '".$flightdata->flightStatuses[0]->arrivalAirportFsCode."'"); echo "&nbsp;".$airport['airport_name']." ,".$airport['country']."(".$airport['iata'].")"; ?>

                                                </div>

                                            </div>

                                            <div class="scheduledtime_box">

                                                <div class="airport_name">Scheduled Time</div>

                                                <div class="byflight_name" style="padding-top: 2px;">:<?php if(isset($flightdata->flightStatuses[0]->operationalTimes->scheduledGateArrival->dateLocal)) 

                                                    {   

                                                        $datetime = preg_split("/[T,]+/", $flightdata->flightStatuses[0]->operationalTimes->scheduledGateArrival->dateLocal);

                                                        

                                                        echo "&nbsp;".date('d M Y, h:i A',strtotime($datetime[0]." ".$datetime[1])).'&nbsp;';

                                                    }

                                                    

                                                    ?>

                                                </div>

                                            </div>

                                            <div class="airport_box">

                                                <div class="airport_name">Actual Time</div>

                                                <div class="byflight_name" style="padding-top: 2px;">:

                                                    <?php if(isset($flightdata->flightStatuses[0]->operationalTimes->actualGateArrival->dateLocal)) 

                                                    {   $datetime = preg_split("/[T,]+/", $flightdata->flightStatuses[0]->operationalTimes->actualGateArrival->dateLocal);

                                                        echo "&nbsp;".date('d M Y, h:i A',strtotime($datetime[0]." ".$datetime[1])).'&nbsp;&nbsp;&nbsp;';

                                                    }

                                                    ?>

                                                </div>

                                            </div>

                                            <div class="scheduledtime_box">

                                                <div class="airport_name">Terminal</div>

                                                <div class="byflight_name" style="padding-top: 2px;">:<?php if(isset($flightdata->flightStatuses[0]->airportResources->arrivalTerminal)) echo "&nbsp;".'Terminal '.$flightdata->flightStatuses[0]->airportResources->arrivalTerminal;

                                                    if(isset($flightdata->flightStatuses[0]->airportResources->arrivalGate))

                                                        echo ' - Gate '.$flightdata->flightStatuses[0]->airportResources->arrivalGate;

                                                ?>



                                                </div>

                                            </div>

                                            <div class="airport_box">

                                                <div class="airport_name">Local Time</div>

                                                <div class="byflight_name" style="padding-top: 2px;">:



                                                    <?php                                                     

                                                    foreach ($flightdata->appendix->airports as $value) {

                                                         

                                                        if($value->iata==$_SESSION['toiata1']) 

                                                        {

                                                            if(isset($value->localTime)) 

                                                            {   $datetime = preg_split("/[T,]+/", $value->localTime);

                                                                echo "&nbsp;".date('d M Y, h:i A',strtotime($datetime[0]." ".$datetime[1])).'&nbsp;&nbsp;&nbsp;';

                                                            }

                                                        }                                                       

                                                    }                                                    

                                                    ?>

                                                </div>

                                            </div>

                                        </div>

                                        <?php 

                                        //echo "</pre>+++++++++++++";

                                        //print_r($flightdata);die;

                                        //SINGLE DATA

                                        if(isset($flightdata->flightStatuses[0]->flightDurations->scheduledBlockMinutes)  )

                                        {

                                            $duration = $flightdata->flightStatuses[0]->flightDurations->scheduledBlockMinutes;



                                            $flightdetails = $flightdata->flightStatuses[0];

                                            /**/

                                            $from_actual_time_RAW = preg_split("/[T,]+/", $flightdata->flightStatuses[0]->operationalTimes->actualGateDeparture->dateLocal);

                                            $from_actual_time = date('d M Y, h:i A',strtotime($from_actual_time_RAW[0]." ".$from_actual_time_RAW[1]));

                                            

                                            $from_local_time = $to_local_time = '';

                                            foreach ($flightdata->appendix->airports as $value) {                                         

                                                if($value->iata==$_SESSION['fromiata1']) 

                                                {

                                                    if(isset($value->localTime)) 

                                                    {   $datetime = preg_split("/[T,]+/", $value->localTime);

                                                        $from_local_time =  date('d M Y, h:i A',strtotime($datetime[0]." ".$datetime[1]));

                                                    }

                                                    break;

                                                }                                       

                                            }



                                            $time_fly = datediff( $from_local_time, $from_actual_time );

                                            $time_fly_my = datediffflytime( $from_local_time, $from_actual_time );



                                            $fly = round(abs(strtotime($from_actual_time) - strtotime($from_local_time)) /60 ,2);

                                            $fly_per = round(($fly * 100) / $duration);

                                            if($fly_per>=100)

                                                $fly_per = 100;

                                            

                                            /********************/



                                            foreach ($flightdata->appendix->airports as $value) {                                         



                                                if($value->iata==$_SESSION['toiata1']) 

                                                {

                                                    if(isset($value->localTime)) 

                                                    {   $datetime = preg_split("/[T,]+/", $value->localTime);

                                                        $to_local_time =  date('d M Y, h:i A',strtotime($datetime[0]." ".$datetime[1]));

                                                    }

                                                    break;

                                                }                                       

                                            }

                                            //print_r($flightdata->flightStatuses[0]->operationalTimes);

                                            //estimatedGateArrival

                                            if($flightdata->flightStatuses[0]->operationalTimes->estimatedGateArrival->dateLocal){

                                                $to_estimated_time_RAW = preg_split("/[T,]+/", $flightdata->flightStatuses[0]->operationalTimes->estimatedGateArrival->dateLocal);

                                                $to_estimated_time = date('d M Y, h:i A',strtotime($to_estimated_time_RAW[0]." ".$to_estimated_time_RAW[1]));

                                                $time_togo = datediff( $to_local_time, $to_estimated_time );

                                            }else{

                                                $to_schedule_time_RAW = preg_split("/[T,]+/", $flightdata->flightStatuses[0]->operationalTimes->scheduledGateArrival->dateLocal);

                                                $to_schedule_time = date('d M Y, h:i A',strtotime($to_schedule_time_RAW[0]." ".$to_schedule_time_RAW[1]));

                                                $time_togo = datediff( $to_local_time, $to_schedule_time );

                                            }//scheduledGateArrival 

                                            

                                            /*-----*/



                                            /*$datetime = preg_split("/[T,]+/", $flightdetails->operationalTimes->actualGateDeparture->dateLocal);

                                            $min1 = date('d M Y, h:i A',strtotime($datetime[0]." ".$datetime[1]));



                                            $datetime = preg_split("/[T,]+/", $flightdetails->operationalTimes->scheduledGateArrival->dateLocal);

                                            $min3 = date('d M Y, h:i A',strtotime($datetime[0]." ".$datetime[1]));



                                            $datetime = preg_split("/[T,]+/", $flightdata->appendix->airports[0]->localTime);

                                            $min2 = date('d M Y, h:i A',strtotime($datetime[0]." ".$datetime[1]));



                                            foreach ($flightdata->appendix->airports as $value) {                                         

                                                if($value->iata==$_SESSION['toiata']) 

                                                {

                                                    if(isset($value->localTime)) 

                                                    {   $datetime = preg_split("/[T,]+/", $value->localTime);

                                                        $minA =  date('d M Y, h:i A',strtotime($datetime[0]." ".$datetime[1]));

                                                    }

                                                    break;

                                                }                                       

                                            }



                                            $fly = round(abs(strtotime($min1) - strtotime($min2)) /60 ,2);

                                            $fly_per = round(($fly * 100) / $duration);

                                            if($fly_per>=100)

                                                $fly_per = 100;



                                            $tot_min1 = round(abs(strtotime($min2) - strtotime($min1)) /60 ,2);

                                            $d = floor ($tot_min1 / 1440);

                                            $hours = floor (($tot_min1 - $d * 1440) / 60);

                                            $minutes = $tot_min1 - ($d * 1440) - ($hours * 60);

                                            $time_fly = $hours. " hrs ".$minutes."  min ";



                                            $tot_min2 = round(abs(strtotime($minA) - strtotime($min3)) /60 ,2);

                                            $d = floor ($tot_min2 / 1440);

                                            $hours = floor (($tot_min2 - $d * 1440) / 60);

                                            $minutes = $tot_min2 - ($d * 1440) - ($hours * 60);

                                            $time_togo = $hours. " hrs ".$minutes."  min ";*/

                                        }

                                        if($flightdata->flightStatuses[0]->status=='A' && $time_fly_my < $duration) { ?>

                                        <div id="flybasediv" class="flight_running">

                                            <div id="flydiv" class="flight_running_track" style="width:<?php echo abs($fly_per).'%';?>;">

                                            </div>    

                                            <br><br>                                    

                                            

                                        </div>

                                        <div style="margin-top: 16.2em;font-size: 15px;">

                                            <?php echo "Flew : " . $time_fly . "  &nbsp &nbsp &nbspTo Go : " . $time_togo;

                                            ?>

                                        </div>

                                        <?php }?>

                                    </div>

                                </div>

                                <div style="float: right;/* background: url(../images/menu1.jpg);*/">

                                    <span id="lbl_enq_flight" class="flight_info" style="color:White;font-family:Arial;font-size:12pt;font-weight:bold;"></span>

                                </div>

                                <?php } ?>

                                <!--container_index End-->

                            </div>

                            <!-- End Flight Detail -->

                            <?php } ?>



                            <!-- Start Route Detail -->

                            <?php 



                                if(isset($_REQUEST['flightdataid']) && $_SESSION['status']=="route" && isset($_REQUEST['detail'])) {



                                $url= 'https://api.flightstats.com/flex/flightstatus/rest/v2/json/flight/status/'.$_REQUEST['flightdataid'].'?appId=2dfe591f&appKey=f5638cdd75d87be1b8214607d90d1f4a';

                                $flightdata = getAPI($url);

                                //echo "<pre>";print_r($flightdata);die();

                                //echo "<pre>---------------";

                                /*print_r($flightdata);*/

                                // ROUTE DATA

                                if(isset($flightdata->flightStatus->flightDurations->scheduledBlockMinutes))

                                {

                                    $duration = $flightdata->flightStatus->flightDurations->scheduledBlockMinutes;



                                    $flightdetails = $flightdata->flightStatus;

                                    /**/

                                    $from_actual_time_RAW = preg_split("/[T,]+/", $flightdetails->operationalTimes->actualGateDeparture->dateLocal);

                                    $from_actual_time = date('d M Y, h:i A',strtotime($from_actual_time_RAW[0]." ".$from_actual_time_RAW[1]));

                                    

                                    $from_local_time = $to_local_time = '';

                                    

                                    foreach ($flightdata->appendix->airports as $value) { 

                                        //echo $value->iata."--*********--".$_SESSION['fromiata'].'--------';

                                        if($value->iata==$_SESSION['fromiata']) 

                                        {

                                            if(isset($value->localTime)) 

                                            {   $datetime = preg_split("/[T,]+/", $value->localTime);

                                                $from_local_time =  date('d M Y, h:i A',strtotime($datetime[0]." ".$datetime[1]));

                                            }

                                            break;

                                        }                                       

                                    }



                                    $time_fly = datediff( $from_local_time, $from_actual_time );

                                    $time_fly_my = datediffflytime( $from_local_time, $from_actual_time );

                                    

                                    $fly = round(abs(strtotime($from_actual_time) - strtotime($from_local_time)) /60 ,2);

                                    $fly_per = round(($fly * 100) / $duration);

                                    if($fly_per>=100)

                                        $fly_per = 100;

                                    

                                    /********************/

                                    foreach ($flightdata->appendix->airports as $value) {                                         



                                        if($value->iata==$_SESSION['toiata']) 

                                        {

                                            if(isset($value->localTime)) 

                                            {   $datetime = preg_split("/[T,]+/", $value->localTime);

                                                $to_local_time =  date('d M Y, h:i A',strtotime($datetime[0]." ".$datetime[1]));

                                            }

                                            break;

                                        }                                       

                                    }

                                    //print_r($flightdata->flightStatuses[0]->operationalTimes);

                                    //estimatedGateArrival

                                    if($flightdetails->operationalTimes->estimatedGateArrival->dateLocal){

                                        $to_estimated_time_RAW = preg_split("/[T,]+/", $flightdetails->operationalTimes->estimatedGateArrival->dateLocal);

                                        $to_estimated_time = date('d M Y, h:i A',strtotime($to_estimated_time_RAW[0]." ".$to_estimated_time_RAW[1]));

                                        $time_togo = datediff( $to_local_time, $to_estimated_time );

                                    }else{

                                        $to_schedule_time_RAW = preg_split("/[T,]+/", $flightdetails->operationalTimes->scheduledGateArrival->dateLocal);

                                        $to_schedule_time = date('d M Y, h:i A',strtotime($to_schedule_time_RAW[0]." ".$to_schedule_time_RAW[1]));

                                        $time_togo = datediff( $to_local_time, $to_schedule_time );

                                    }//scheduledGateArrival 

                   //echo $from_local_time.'+++++++++++'.$from_actual_time;                 

                                    /*-----*/

                                    /*$datetime = preg_split("/[T,]+/", $flightdetails->operationalTimes->actualGateDeparture->dateLocal);

                                    $min1 = date('d M Y, h:i A',strtotime($datetime[0]." ".$datetime[1]));





                                    $datetime = preg_split("/[T,]+/", $flightdetails->operationalTimes->scheduledGateArrival->dateLocal);

                                    $min3 = date('d M Y, h:i A',strtotime($datetime[0]." ".$datetime[1]));



                                    $datetime = preg_split("/[T,]+/", $flightdata->appendix->airports[0]->localTime);

                                    $min2 = date('d M Y, h:i A',strtotime($datetime[0]." ".$datetime[1]));

                                    

                                    foreach ($flightdata->appendix->airports as $value) {                                         

                                        if($value->iata==$_SESSION['toiata']) 

                                        {

                                            if(isset($value->localTime)) 

                                            {   $datetime = preg_split("/[T,]+/", $value->localTime);

                                                $minA =  date('d M Y, h:i A',strtotime($datetime[0]." ".$datetime[1]));

                                            }

                                            break;

                                        }                                       

                                    }

                                    

                                    $fly = round(abs(strtotime($min1) - strtotime($min2)) /60 ,2);

                                    $fly_per = round(($fly * 100) / $duration);

                                    if($fly_per>=100)

                                        $fly_per = 100;



                                    $tot_min1 = round(abs(strtotime($min2) - strtotime($min1)) /60 ,2);

                                    if(isset($flightdetails->delays->departureGateDelayMinutes) && $flightdetails->delays->departureGateDelayMinutes > 0)

                                        $tot_min1 -= $flightdetails->delays->departureGateDelayMinutes;

                                    $d = floor ($tot_min1 / 1440);

                                    $hours = floor (($tot_min1 - $d * 1440) / 60);

                                    $minutes = $tot_min1 - ($d * 1440) - ($hours * 60);

                                    

                                         

                                    $time_fly = $hours. " hrs ".$minutes."  min ";



                                    $tot_min2 = round(abs(strtotime($minA) - strtotime($min3)) /60 ,2);

                                    $d = floor ($tot_min2 / 1440);

                                    $hours = floor (($tot_min2 - $d * 1440) / 60);

                                    $minutes = $tot_min2 - ($d * 1440) - ($hours * 60);

                                    $time_togo = $hours. " hrs ".$minutes."  min ";*/

                                    

                                }

                            ?>
                            <?php
                                $codeshareAry =array();
                                if($flightdetails->codeshares && count($flightdetails->codeshares)>0){
                                    foreach($flightdetails->codeshares as $valval){
                                        $ALtmp = getRow('tbl_airlines',null,"`icao` LIKE '".$valval->fsCode."'");
                                        $codeshareAry[] = $ALtmp['airline_name'];
                                        if($valval->relationship=='X'){
                                            $flightdetails->carrierFsCode = $valval->fsCode;
                                            $flightdetails->flightNumber = $valval->flightNumber;
                                        }
                                    }                                                    
                                } 
                            ?>
                                

                            <div class="contant_container" style="margin-left: -460px;margin-top: -100em;">

                                <div style="float: right;margin-top: -60px/* background: url(../images/menu1.jpg);*/">

                                    <form action="index.php" method="post">

                                        <input type="hidden" name="byroute" id="byroute" value="<?php echo $_REQUEST['byroute'];?>" />

                                        <input type="hidden" name="byarrivalroute" id="byarrivalroute" value="<?php echo $_REQUEST['byarrivalroute'];?>" />

                                        <input type="hidden" name="routedate" id="routedate" value="<?php echo $_REQUEST['routedate'];?>" />

                                        <?php if(!empty($_REQUEST['airline'])) {?>

                                         <input type="hidden" name="airline" id="airline" value="<?php echo $_REQUEST['airline'];?>" />

                                         <?php } ?>

                                        <div style="float: right; margin-top: 11px">

                                            <input id="findflights" class="find_flights toggle_enquiry" type="submit" value="<< Back " name="findflights" style="display: inline;">

                                        </div>

                                    </form>

                                    <br><br><br><br><br>

                                    <span id="lbl_enq_flight" class="flight_info" style="color:White;font-family:Arial;font-size:12pt;font-weight:bold;">

                                        <?php $airline = getRow('tbl_airlines',null,"`icao` LIKE '".$flightdetails->carrierFsCode."'");?>

                                        <span style="color:Yellow">

                                           <?php echo $airline['airline_name'];?>

                                        </span>

                                        | Flight Number 

                                        <span style="color:Yellow">

                                            <?php echo $flightdetails->flightNumber.' ('.$flightdetails->carrierFsCode.$flightdetails->flightNumber.')';?>

                                        </span>

                                    </span>

                                </div>
                                <div style="float: right;margin-top: 10px;">
                                <span style="color:white;font-family:Arial;font-size:9pt;float:right;font-weight:bold;">
                                <?php
                                    if(count( $codeshareAry)>0){
                                        echo '['.implode(" , ", $codeshareAry).']';    
                                    }
                                    
                                ?>
                                </span>                                                          
                                <div>
                           

                                <div id="panel_flight">

                                    <div class="container_byflight_inner">

                                        <div class="container_byflight_box">

                                            <div class="byflight_box">

                                                <div class="byflight_departure">Departure</div>

                                            </div>

                                            <div class="status">

                                                <div class="byflight_departure">

                                                    <div class="arrival_txt" style="float: right;">

                                                        <span id="lbl_flight_status"><?php if(isset($flightdetails->delays->departureGateDelayMinutes) && $flightdetails->delays->departureGateDelayMinutes > 10) echo 'Takeoff '; ?></span>

                                                        <span id="lbl_late_status" style="color:Yellow"><?php if(isset($flightdetails->delays->departureGateDelayMinutes) && $flightdetails->delays->departureGateDelayMinutes > 10) 

                                                            {   echo convertToHoursMins($flightdetails->delays->departureGateDelayMinutes).' min Late';

                                                            }

                                                            ?>

                                                        </span>

                                                    </div>

                                                </div>

                                            </div>

                                            <div class="airport_box">

                                                <div class="airport_name">Airport</div>

                                                <?php $airport = getRow('tbl_airport',null,"iata LIKE '".$flightdetails->departureAirportFsCode."'");

                                                $airportname_departure = $airport['airport_name']."  ".$airport['country']."(".$airport['iata'].")"; 



                                                $airport = getRow('tbl_airport',null,"iata LIKE '".$flightdetails->arrivalAirportFsCode."'"); 

                                                $airportname_arrival = $airport['airport_name']."  ".$airport['country']."(".$airport['iata'].")"; 

                                                 ?>

                                                <?php if(strlen($airportname_departure) > 50 || strlen($airportname_arrival) > 50) { ?>

                                                    <div class="byflight_name" style="padding-top: 2px;height: 38px;">&nbsp;<?php echo "&nbsp;".$airportname_departure; ?>

                                                    </div>

                                                <?php } else { ?>

                                                    <div class="byflight_name" style="padding-top: 2px;">&nbsp;<?php echo "&nbsp;".$airportname_departure; ?>

                                                    </div>

                                                <?php } ?>

                                            </div>



                                            <div class="scheduledtime_box">

                                                <div class="airport_name">Scheduled Time</div>

                                                <div class="byflight_name" style="padding-top: 2px;">&nbsp;<?php if(isset($flightdetails->operationalTimes->scheduledGateDeparture->dateLocal)) 

                                                    {   $datetime = preg_split("/[T,]+/", $flightdetails->operationalTimes->scheduledGateDeparture->dateLocal);

                                                        echo "&nbsp;".date('d M Y, h:i A',strtotime($datetime[0]." ".$datetime[1])).'&nbsp;';

                                                    }

                                                   if(isset($flightdetails->delays->departureGateDelayMinutes) && $flightdetails->delays->departureGateDelayMinutes > 10)

                                                    {

                                                        if(isset($flightdetails->operationalTimes->estimatedGateDeparture->dateLocal)) 

                                                        {   $datetime = preg_split("/[T,]+/", $flightdetails->operationalTimes->estimatedGateDeparture->dateLocal);

                                                            echo "&nbsp;"."<span style='font-size:13px;'><img title='Expected Departure Time' height='16' src='images/expected.png' />[".date('d M Y, h:i A',strtotime($datetime[0].' '.$datetime[1])).']<span>';

                                                        }

                                                    }

                                                    ?>

                                                </div>

                                            </div>

                                            <div class="airport_box">

                                                <div class="airport_name">Actual Time</div>

                                                <div class="byflight_name" style="padding-top: 2px;">&nbsp;<?php if(isset($flightdetails->operationalTimes->actualGateDeparture->dateLocal)) 

                                                    {   $datetime = preg_split("/[T,]+/", $flightdetails->operationalTimes->actualGateDeparture->dateLocal);

                                                        echo "&nbsp;".date('d M Y h:i A',strtotime($datetime[0]." ".$datetime[1])).'&nbsp;&nbsp;&nbsp;';

                                                    }

                                                    ?>

                                                </div>

                                            </div>

                                            <div class="scheduledtime_box">

                                                <div class="airport_name">Terminal</div>

                                                <div class="byflight_name" style="padding-top: 2px;">&nbsp;

                                                    <?php if(isset($flightdetails->airportResources->departureTerminal)) echo 'Terminal '.$flightdetails->airportResources->departureTerminal;

                                                        if(isset($flightdetails->airportResources->departureGate)) echo ' Gate-'.$flightdetails->airportResources->departureGate;

                                                    ?>

                                                </div>

                                            </div>

                                            

                                            <div class="airport_box">

                                                <div class="airport_name">Local Time</div>

                                                <div class="byflight_name" style="padding-top: 2px;">&nbsp;

                                                    <?php                                                     

                                                    foreach ($flightdata->appendix->airports as $value) {

                                                         

                                                        if($value->iata==$_SESSION['fromiata']) 

                                                        {

                                                            if(isset($value->localTime)) 

                                                            {   $datetime = preg_split("/[T,]+/", $value->localTime);

                                                                echo "&nbsp;".date('d M Y h:i A',strtotime($datetime[0]." ".$datetime[1])).'&nbsp;&nbsp;&nbsp;';

                                                            }

                                                        }                                                       

                                                    }                                                    

                                                    ?>

                                                </div>

                                            </div>

                                        </div>

                                        <div class="container_byflight_box" style="margin-right: 0px; float: right;">

                                            <div class="byflight_box">

                                                <div class="byflight_departure">Arrival</div>

                                            </div>

                                            <div class="status">

                                                <div class="byflight_departure">

                                                    <div class="arrival_txt" style="float: right;">

                                                        <span id="lbl_flight_status"><?php if($flightdetails->status=='L') echo 'Landed'; else if($flightdetails->status=='A') echo 'Active'; else if($flightdetails->status=='U') echo 'Unknown'; else if($flightdetails->status=='C') echo 'Canceled';else echo 'Scheduled';?></span>

                                                        <span id="lbl_late_status" style="color:Yellow"><?php if(isset($flightdetails->delays->arrivalGateDelayMinutes) && $flightdetails->delays->arrivalGateDelayMinutes > 10 && $flightdetails->status!='S') 

                                                            echo convertToHoursMins($flightdetails->delays->arrivalGateDelayMinutes).' min Late';?>

                                                        </span>

                                                    </div>

                                                    <div class="delayed_round" style="float: right;">

                                                        <?php if($flightdetails->status=='L') {?>

                                                        <img id="img_flight_status" src="images/round_landed.png" style="border-width:0px;">
                                                        <?php }else if($flightdetails->status=='C') { ?>

                                                        <img id="img_flight_status" src="images/round_cancelled.png" style="border-width:0px;">                                                        
                                                        <?php } else { ?>

                                                        <img id="img_flight_status" src="images/round_other.png" style="border-width:0px;">

                                                        <?php } ?>

                                                    </div>

                                                    <div class="arrival_txt" style="float: right; ">Flight Status:  </div>

                                                </div>

                                            </div>

                                            <div class="airport_box">

                                                <div class="airport_name">Airport</div>

                                                 <?php if(strlen($airportname_departure) > 50 || strlen($airportname_arrival) > 50) { ?>

                                                <div class="byflight_name" style="padding-top: 2px;height: 38px;">&nbsp;<?php echo "&nbsp;".$airportname_arrival; ?>

                                                </div>

                                                <?php } else { ?>

                                                <div class="byflight_name" style="padding-top: 2px;">&nbsp; <?php echo "&nbsp;".$airportname_arrival; ?>

                                                </div>

                                                <?php } ?>

                                            </div>

                                            <div class="scheduledtime_box">

                                                <div class="airport_name">Scheduled Time</div>

                                                <div class="byflight_name" style="padding-top: 2px;">&nbsp;<?php if(isset($flightdetails->operationalTimes->scheduledGateArrival->dateLocal)) 

                                                    {   $datetime = preg_split("/[T,]+/", $flightdetails->operationalTimes->scheduledGateArrival->dateLocal);

                                                        echo "&nbsp;".date('d M Y, h:i A',strtotime($datetime[0]." ".$datetime[1])).'&nbsp;';

                                                    }

                                                    if(isset($flightdetails->delays->arrivalGateDelayMinutes) && $flightdetails->delays->arrivalGateDelayMinutes > 10)

                                                    {

                                                        if(isset($flightdetails->operationalTimes->estimatedGateArrival->dateLocal)) 

                                                        {   $datetime = preg_split("/[T,]+/", $flightdetails->operationalTimes->estimatedGateArrival->dateLocal);

                                                            echo "&nbsp;"."<span style='font-size:13px;'><img title='Expected Arrival Time' height='16' src='images/expected.png' />[".date('d M Y, h:i A',strtotime($datetime[0].' '.$datetime[1])).']<span>';

                                                        }

                                                    }

                                                    ?>

                                                </div>

                                            </div>

                                            <div class="airport_box">

                                                <div class="airport_name">Actual Time</div>

                                                <div class="byflight_name" style="padding-top: 2px;">&nbsp;<?php if(isset($flightdetails->operationalTimes->actualGateArrival->dateLocal))

                                                    {   $datetime = preg_split("/[T,]+/", $flightdetails->operationalTimes->actualGateArrival->dateLocal);

                                                        echo "&nbsp;".date('d M Y h:i A',strtotime($datetime[0]." ".$datetime[1])).'&nbsp;&nbsp;&nbsp;';

                                                    }

                                                    ?>

                                                </div>

                                            </div>

                                            <div class="scheduledtime_box">

                                                <div class="airport_name">Terminal</div>

                                                <div class="byflight_name" style="padding-top: 2px;">&nbsp;

                                                    <?php if(isset($flightdetails->airportResources->arrivalTerminal)) echo 'Terminal '.$flightdetails->airportResources->arrivalTerminal;

                                                        if(isset($flightdetails->airportResources->arrivalGate)) echo ' Gate '.$flightdetails->airportResources->arrivalGate;                                                   

                                                ?>

                                                </div>

                                            </div>



                                            <div class="airport_box">

                                                <div class="airport_name">Local Time</div>

                                                <div class="byflight_name" style="padding-top: 2px;">&nbsp;

                                                    <?php 

                                                    

                                                    foreach ($flightdata->appendix->airports as $value) {

                                                         

                                                        if($value->iata==$_SESSION['toiata']) 

                                                        {

                                                            if(isset($value->localTime)) 

                                                            {   $datetime = preg_split("/[T,]+/", $value->localTime);

                                                                echo "&nbsp;".date('d M Y h:i A',strtotime($datetime[0]." ".$datetime[1])).'&nbsp;&nbsp;&nbsp;';

                                                            }

                                                        }                                                       

                                                    }                                                    

                                                    ?>

                                                    

                                                </div>

                                            </div>

                                        </div>                                  

                                        <?php if($flightdetails->status=='A' && $time_fly_my < $duration ) { ?>

                                        <div id="flybasediv" class="flight_running">

                                            <div id="flydiv" class="flight_running_track" style="width:<?php echo abs($fly_per).'%';?>;">

                                            </div>    

                                            <br><br>                                    

                                            

                                        </div>

                                        <div style="margin-top: 16.2em;font-size: 15px;">

                                            <?php echo "Flew : " . $time_fly . "  &nbsp &nbsp &nbspTo Go : " . $time_togo;

                                            ?>

                                        </div>

                                        <?php }?>

                                    </div>

                                </div>

                                <div style="float: right;/* background: url(../images/menu1.jpg);*/">

                                    <span id="lbl_enq_flight" class="flight_info" style="color:White;font-family:Arial;font-size:12pt;font-weight:bold;"></span>

                                </div>

                                <!--container_index End-->

                            </div>

                            <?php } ?>

                            <!-- End Route Detail -->

                        </div>

                        <!--container end-->

                    </center>

                </div>

            </div>

        </div>

    </div>

    <!--footer-->

    <div id="air_bin_footer" style="position: fixed;">

    <!--    <span>© 2014 www.airbing.com. All rights reserved.</span> -->

    </div>

    <script type="text/javascript" src="js/datepickr.js"></script>  

    <script type="text/javascript">

        

        new datepickr('flightdate', {

            'dateFormat': 'd-F-Y'

        });



        new datepickr('routedate', {

            'dateFormat': 'd-F-Y'

        });

    </script>

    <?php //echo $flightdata->flightStatus->flightId;?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-58129725-1', 'auto');
  ga('send', 'pageview');

</script>
</body>

</html>

<?php

function datediff( $date1, $date2 )

{

    $diff = abs( strtotime( $date1 ) - strtotime( $date2 ) );

    return sprintf

    (

        " %d Hours, %d Mins",        

        intval( ( $diff % 86400 ) / 3600),

        intval( ( $diff / 60 ) % 60 )        

    );

}



function datediffflytime( $date1, $date2 )

{

    $diff = abs( strtotime( $date1 ) - strtotime( $date2 ) );

    

        

    $hours = intval( ( $diff % 86400 ) / 3600)*60;//10

    $minutes = intval( ( $diff / 60 ) % 60 ); //14

    return $hours+$minutes;

}

?>