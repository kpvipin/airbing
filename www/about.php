<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta http-equiv="cache-control" content="max-age=0" />

<meta http-equiv="cache-control" content="no-cache" />

<meta http-equiv="expires" content="0" />

<meta http-equiv="pragma" content="no-cache" />

<title>Airbing : About Us</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />

<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<script src="https://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>

<script type="text/javascript">

/*jQuery(document).ready(function(){ 

  jQuery("#airline").easyTooltip();

});*/

</script>

<style type="text/css">

a.info {

     position:absolute; /*this is the key*/

     z-index:0; 

     color:#666;

     font-style:oblique;

     text-decoration:none;  

     display:block;

     float:left;

 }

a.info .ttip {

     display: none;

     float:left;

 }

a.info:hover{

     z-index:1; 

     background-color:#fff;

 }

a.info:hover .ttip {  

     display:block;

     position:absolute;

     top:20px; 

     left:40px; 

     width:120px;

     border:3px double #0cf;

     background-color:#cff; 

     color:#099;

     font-size:10px;

     font-style:normal;

     padding:5px;

 }

#pic1 {

     background-image:url(images/fstatus.png);

     /*border:3px solid #060;*/

     width:105px;

     height:100px;

}

#pic2 {

    background-image:url(images/fstatus.png);

    /*border:3px solid #060;*/

    width:105px;

    height:100px;

}

#pic3 {

    background-image:url(images/fstatus.png);

    /*border:3px solid #060;*/

    width:105px;

    height:100px;

}

a.info:hover #pictip {

     top:110px; 

     left:10px; 

     width:240px;

     border:3px double #fc0;

     background-color:#ffc; 

     color:#990;

     font-size:12px;

     text-align:justify;

     padding:10px;

 }

 /*Second tooltip*/

 a.tooltip {  outline:none; } 

 a.tooltip strong {  line-height:30px;} 

 a.tooltip:hover {text-decoration:none;} 

 a.tooltip span 

 { z-index:10;display:none; padding:14px 20px; margin-top:24px; margin-left:-170px; width:300px; line-height:16px; 

 } 

 a.tooltip:hover span

 { display:inline; position:absolute; border:2px solid #FFF; color:#EEE; background:#333 url(images/css-tooltip-gradient-bg.png) repeat-x 0 0; 

 } 

 .callout {  z-index:20;position:absolute;border:0;top:-14px;left:120px;} 

 /*CSS3 extras*/ 

 a.tooltip span { border-radius:2px; box-shadow: 0px 0px 8px 4px #666; /*opacity: 0.8;*/ }



.about

{ width: 85px;

  float: left;

  height: 80px;

  font-family: Arial,Helvetica,sans-serif;

  font-size: 13px;

  font-weight: normal;

  color: #000;

  background: none repeat scroll 0% 0% #F2F2F2;

  padding: 10px;

  border-radius: 15px;

  margin-left: 2em;

}

</style>

</head>

 

<body id="body_id">

<!-- Start Header -->

<div id="air_bin_header">

  <div id="main_logo"> <img alt="" src="images/logo.png" width="211" height="70" /></div>

  <div id="air_bin_menu" style="margin-right:6%;width:204px;">

    <ul>

      <li style="line-height:0px; font-weight:bold; padding-top:0px;"><a title="Give Your Feedback"  href="feedback.php"><img src="images/feedback.png" src="images/your_voice.png" style="margin-bottom:5px; margin-top:20px;" /><br/>FEEDBACK</a></li>

      <li style="line-height:0px; font-weight:bold; padding-top:0px;"><a title="Home" style="cursor:pointer;" href="index.html"><img src="images/home.png" style="margin-bottom:5px; margin-top:20px;" /><br/>HOME</a></li>

    </ul>

    <div id="PanelVoice"> <br />

      <!-- <div id="voice_button" >

                    <a href="yourposts.php">

                        <img src="images/button_count.jpg"  style ="margin-top: 10px;border-top-left-radius: 4px;border-top-right-radius: 4px;border-bottom-right-radius: 4px;border-bottom-left-radius: 4px;"  /></a>

                </div>--> 

    </div>

  </div>

</div>

<!-- End Header -->

<div id="air_bin_contant_area_mainx">

  <div id="air_bin_contant_area">

    <div id="air_bin_form">

      <div id="wapper">

        <center>

          <div class="container" style="margin-top:30%;">



            <div class="about">

              <div>

                <table width="100%" >

                  <tr>

                      <!-- Second Tooltip -->

                      <td>

                        <a href="#" class="tooltip" style="cursor:pointer"> <img src="images/about1.png" width="75" height="75" /> 

                          <span style='font-size:18px;font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;'> <img class="callout" src="images/callout_black.gif" /> 

                            <strong>About</strong><br /> 

                            <hr>

                          Airbing was founded to assist airline passengers with their travel needs. We stand next to our users, just a tap away. We are on a mission to provide the best service possible on a variety of your travel needs. We started with a few portfolio services and we are looking forward to expand with more that add value to airline passengers like you. In this pursuit we like to have your support and invite your suggestions. Please use our feedback session to communicate with us. Let us know what new services or changes you would like to see with us.</span> 

                        </a>

                      </td>

                  </tr>

                </table>

              </div>

            </div>

            <div class="about">

              <div>

                <table width="100%" >

                  <tr>

                      <td>

                        <a href="#" class="tooltip" style="cursor:pointer"> <img src="images/fstatus.png" width="75" height="75" /> 

                          <span style='font-size:18px;font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;'> <img class="callout" src="images/callout_black.gif" /> 

                            <strong>Flight Status</strong><br /> 

                            <hr>

                          This is ubiquitous. We at Airbing want to make it easy for people of all ages. No more fumbling through confusing websites. Take advantage of our site.</span> 

                        </a>

                      </td>

                  </tr>

                </table>

              </div>

            </div> 

             <div class="about">

              <div>

                <table width="100%" >

                  <tr>

                      

                      <td>

                        <a href="#" class="tooltip" style="cursor:pointer"> <img src="images/reviewicon.png" width="75" height="75" /> 

                          <span style='font-size:18px;font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;'> <img class="callout" src="images/callout_black.gif" /> 

                            <strong>Your Voice</strong><br /> 

                            <hr>

                             Air line travel is one of those types of transport that could be either hated or loved. Unfortunately many experience harrowing travel situations, but unable to express and bring to the attention of the respective personnel. Airbing provides a platform to share your experiences so that people on the other side, Airlines, could take necessary actions if it is within their control. We want your honest experiences, sharing it could trigger follow up actions from fellow passengers being cognizant of an issue to real solutions making the world of travel a better place.</span> 

                        </a>

                      </td>

                  </tr>

                </table>

              </div>

            </div> 

            <!--HEADER -->

            <div class="wait_preload" style="display: none;"> <img src="images/wait.gif" /> </div>

            <!--HEADER END--> 

          </div>

          <!--container end-->

        </center>

      </div>

    </div>

  </div>

</div>

<!--footer-->
 <div id="air_bin_footer" style="-webkit-font-smoothing: antialiased;background-color:#4E4E6E;">

       <!--  <a href="http://www.airbing.com/terms.html" onclick="javascript:void window.open('http://airbing.com/terms.html','1435232951271','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Terms of Use</a> -->

        <span id="dialog_trigger" onclick="popupwindow('http://airbing.com/terms.html', 'Terms and Conditions', 700, 700);" style="cursor:pointer;"><strong>Terms of Use</strong></span>
        &nbsp;&nbsp;&nbsp;
        <span id="dialog_trigger2" onclick="popupwindow('http://airbing.com/privacy.html', 'Terms and Conditions', 700, 700);"  style="cursor:pointer;"><strong>Privacy</strong></span>

        
    </div>  
<div id="dialog" style="display:none;" title="Terms and Conditions"><iframe frameborder="0" scrolling="yes" width="700" height="700" src="http://www.airbing.com/terms.html"></iframe></div>

<div id="dialog2" style="display:none;" title="Privacy Policy"><iframe frameborder="0" scrolling="yes" width="700" height="700" src="http://www.airbing.com/privacy.html"></iframe></div>

 <script>
function popupwindow(url, title, w, h) {
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
} 
/*$(document).ready(function(){ 

  $( "#dialog_trigger" ).click(function() {
    $( "#dialog" ).dialog( "open" );
  });
  $("#dialog").dialog({
      autoOpen: false,
      position: 'center' ,
      title: 'Terms and Conditions',
       draggable: false,
    width : 800,
    height : 700, 
    resizable : true,
    modal : true,
  });


  $( "#dialog_trigger2" ).click(function() {
    $( "#dialog2" ).dialog( "open" );
  });
  $("#dialog2").dialog({
      autoOpen: false,
      position: 'center' ,
      title: 'Privacy Policy',
      draggable: false,
      width : 800,     
      height: "auto", 
      resizable : false,
      modal : true
  });

});*/


</script>

 <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-58129725-1', 'auto');
  ga('send', 'pageview');

</script>

</body>

</html>