<?php
session_start();
include("connection.php");
function IsAdmin()
    {
        if(!isset($_SESSION['adminid']) || $_SESSION['adminid'] =='' ){         
            unset($_SESSION);
            header('Location:../login.php');
        }
    }

function IsCompany()
    {
        if(!isset($_SESSION['compid']) || $_SESSION['compid'] =='' ){         
            unset($_SESSION);
            header('Location:../login.php');
        }
    }


function AnyDataSelect($table,$fieldname='',$where='',$limit='')
    {
        $fieldname = ($fieldname=='') ? ' * ' : $fieldname ;
        $where = ($where=='') ? ' WHERE 1 = 1' :  " WHERE " .$where ;
        $limit = ($limit=='') ? '  ' : $limit ;
        
        $sql = "SELECT ".$fieldname." FROM ".$table." ".$where."  ".$limit;
        //echo $sql;
        $result = mysql_query($sql) or die("Error at Line : ".__LINE__);
        $ARY = array();
        if(mysql_num_rows($result)>0){
            while($row = mysql_fetch_assoc($result) ){
                $ARY[] = $row;
            }//while($row = mysql_fetch_assoc($result) )
        }//if(mysql_num_rows($result)>0)
        return $ARY;
    }

    function get_menu($menu)
    {
        $menuname= "";
        $filename = basename($_SERVER['PHP_SELF']);
        $tmp = stripos($filename, $menu);
        if ($tmp  === 0) {
          $menuname = "active";
        }
        return  $menuname;
    }

function getSignature(){
        
        $timestamp = time();
        $nonce = rand();
        $signature_string = 'app_id='.APPLICATION_ID.'&auth_key='.AUTH_KEY.'&nonce='.$nonce.'&timestamp='.$timestamp;
        $signature = hash_hmac('sha1', $signature_string , AUTH_SECRET);    
        
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, 'https://api.quickblox.com/session.json'); // Full path is - https://api.quickblox.com/session.json
        curl_setopt($curl, CURLOPT_POST, true); // Use POST
        curl_setopt($curl, CURLOPT_POSTFIELDS, $signature_string.'&signature='.$signature); // Setup post body
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Receive server response
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // Receive server response

        // Execute request and read response
        $response = curl_exec($curl);

        $json_response = urldecode(stripslashes(preg_replace("#(/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|([\s\t]//.*)|(^//.*)#", '', $response)));
        $data_json_response = json_decode($json_response, true);

        $data_json_response['session']['timestamp'] = $timestamp;
        return $data_json_response['session'];
    }

    function registerQB($email,$fullname){
        // Sanitize uname
       // $username = preg_replace("/[^a-zA-Z0-9.]+/", "",$username);
    
        $SiggiData = getSignature();
    
        // Build post body
        $post_body = http_build_query(array(
            'application_id' => APPLICATION_ID,
            'auth_key' => AUTH_KEY,
            'timestamp' => $SiggiData['timestamp'],
            'nonce' => $SiggiData['nonce'],
            'token' => $SiggiData['token'],
            'signature' => $SiggiData['_id'],
            'user[password]' => '0123456789',
	    'user[full_name]' => $fullname,
            'user[email]' => $email,
            'user[login]' => $email
        ));
    
        // Configure cURL
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT . '/' . QB_PATH_SESSION); // Full path is - https://api.quickblox.com/session.json
        curl_setopt($curl, CURLOPT_POST, true); // Use POST
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body); // Setup post body
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Receive server response
        curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,10); # timeout after 10 seconds, you can increase it
        curl_setopt($curl,CURLOPT_HEADER,false);
        curl_setopt($curl,  CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)"); # Some server may refuse your request if you dont pass user agent
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    
        // Execute request and read response
        $response = curl_exec($curl);
        curl_close($curl);       
        // Check errors
        if ($response) {    
            $result = json_decode($response,true);

            if (!empty($result['errors'])) {    
            //print_r($result['errors']);
                return 0;
            }else{
                return $result['user']['id'];
            }
        } else {
            return 0;
        }
    }

    function CheckLogin($request)
    {
        extract($request);
        if(isset($email) && $email != '' &&
         isset($password) && $password != '')
        {
            $sel = "SELECT * FROM login where email like '".mysql_real_escape_string($email)."' AND password like '".mysql_real_escape_string(md5($password))."' AND is_active = 1 AND type IN ('SA', 'B') ";
            $result = mysql_query($sel) or die(mysql_error());
            if(mysql_num_rows($result)>0)
            {
                $row = mysql_fetch_assoc($result);
                extract($row);
                if($type == 'SA')
                {
                    $_SESSION['adminid'] = $id;
                    $_SESSION['email'] = $email;
                    $_SESSION['first_name'] = $first_name;
                    header("location:admin/home.php"); 
                }
                if($type == 'B')
                {
                    $_SESSION['compid'] = $id;
                    $_SESSION['c_name'] = $business_name;
                    $_SESSION['email'] = $email;
                    if($employer == '1')
                    {
                        $_SESSION['employer_check'] = 'true';  
                    }
                    $sel_cmp = "SELECT id FROM company WHERE login_id = '".$_SESSION['compid']."' ";
                    $result_cmp = mysql_query($sel_cmp) or die(mysql_error().__LINE__);
                    $row_cmp = mysql_fetch_array($result_cmp);
                    extract($row_cmp);
                    $_SESSION['c_id'] = $id;
                    header("location:company/home.php"); 
                }
                
            }
            else 
            {
               header("location:login.php?error=true");
            }       
        }   
        
    }
    function p($data){
        echo "<pre/>";
        print_r($data);
        die;
    }
    
    function v($data){
        vr_dump($data);
        die;
    }
    
    function checkUser(){
        @session_start();
//      print_r($_SESSION);die;
        if(!isset($_SESSION['id']) || $_SESSION['id']=='' || $_SESSION['type']=='U'){
            return false;           
        }
        return true;        
    }
?>
