<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cossist : Homepage</title>
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.transitions.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    
</head><!--/head-->

<body id="home" class="homepage">

    <header id="header">
        <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><img src="images/logo.png" height="85" alt="logo"   ></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="scroll active"><a href="index.php">Home</a></li>
                        <li class="scroll"><a href="about.php">About</a></li>
                        <li class="scroll "><a href="login.php">Login</a></li> 
                        <li class="scroll"><a href="signup.php">Signup</a></li>                           
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->

    <section id="main-slider">
        <div class="owl-carousel" >
            <div class="item" style="background-image: url(images/slider/1600X550_1.png);">
				<div class="slider-inner">
                    <div class="container">
                        <div class="row" style="color:black;">
                            <div class="col-sm-6">
                                <div class="carousel-content" >
                                    <h2><div></div></h2><h3>Really, who is your friend ?</h3>
                                    <h3>Cossist redefines friendship</h3>
                                    <p>As easy as it can be. Unconditional , Unwavering, and Undemanding.</p>
                                    <p style="font-size:17px;">We assist you with any quries and empower you to have a productive instantaneous conversation with neighborhood businesses, customers, employers, vendors, and global brands alike all from your finger tips.</p>
								</div>
							</div>
						 </div>
						 <br />
						<a href="#" target="_blank" ><img src="images/android.png" alt=""></a>
						<a href="#" target="_blank" ><img src="images/ios_cs.png" alt=""></a>
					</div>
                </div>

            </div><!--/.item-->
             <div class="item" style="background-image: url(images/slider/1600X550_2.png);">
                <div class="slider-inner">
                    <div class="container">
                        <div class="row"  style="color:black;">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <h2><div></div></h2><h3>Built To Last</h3>
                                    <p style="font-size:17px;">We make possible 360  degree communication with your employer. Not only that your regular customer service channel is redefined, you will see unprecedented and innovative adhoc group chats, taking the definition of communication to a new level.</p>
								</div>
                            </div>
                        </div>
                         <br />
						<a href="#" target="_blank" ><img src="images/android.png" alt=""></a>
						<a href="#" target="_blank" ><img src="images/ios_cs.png" alt=""></a>
                    </div>
                </div>
            </div><!--/.item-->
            <div class="item" style="background-image: url(images/slider/1600X550_1.png);">
                <div class="slider-inner">
                    <div class="container">
                        <div class="row"  style="color:black;">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <h2><div></div></h2><h3>Built To Serve</h3>                                    
                                    <p style="font-size:17px;">Cossit was built to cater for a variety of business and organizations. Imagination is the only constraint how you can leverage our services. Some of the areas where Cossit will make a difference are schools, Doctor offices, Restaurents, Police stations, Government offices, Strip malls and the list goes on and is  countless.....</p>
								</div>
                            </div>
                        </div>
                         <br />
						<a href="#" target="_blank" ><img src="images/android.png" alt=""></a>
						<a href="#" target="_blank" ><img src="images/ios_cs.png" alt=""></a>
                    </div>
                </div>
            </div><!--/.item-->
            <div class="item" style="background-image: url(images/slider/1600X550_4.png);">
                <div class="slider-inner">
                    <div class="container">
                        <div class="row"  style="color:black;">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <h2><div></div></h2><h3>Built To Roll</h3>                                    
                                    <p style="font-size:17px;">
We provide maximum flexibilty to your chat. Selecting from a variety of media files to selecting inidvidual agents ( if the receiving party allows). Locaton based service helps you to quickly figure out nearby businesses where you can strike instantaneous conversations.</p>  
<p style="font-size:17px;">
You didnt find what you are looking for ?  No worries, Cossist have  you convered  with our own agents. Our agents can hang around with you for a friendly chat to a chat that could provide you with invaluable information.</p>
								</div>
                            </div>
                        </div>
                         <br />
						<a href="#" target="_blank" ><img src="images/android.png" alt=""></a>
						<a href="#" target="_blank" ><img src="images/ios_cs.png" alt=""></a>
                    </div>
                </div>
            </div><!--/.item-->
              
        </div><!--/.owl-carousel-->
    </section><!--/#main-slider-->

    <section id="get-in-touch">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Get Application on Store</h2>
                <p class="text-center wow fadeInDown">
                    <a href="#" target="_blank" ><img src="images/android.png" alt=""></a>
                    <a href="#" target="_blank" ><img src="images/ios_cs.png" alt=""></a>
                </p>
            </div>
        </div>
    </section><!-- -->

      <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2015 Your Company
                </div>
                <div class="col-sm-6">
                    <ul class="social-icons">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        <li><a href="#"><i class="fa fa-behance"></i></a></li>
                        <li><a href="#"><i class="fa fa-flickr"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-github"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="js/owl.carousel.min.js"></script>
    <script src="js/mousescroll.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/jquery.inview.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/main.js"></script>
</body>
</html>