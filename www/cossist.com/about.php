<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cossist : About Us</title>
    <!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.transitions.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body id="home" class="homepage">

    <header id="header">
        <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="logo">Cosist</a> 
                -->
                     <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="logo"   ></a>
                </div>
                
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                         <li class="scroll"><a href="index.php">Home</a></li>
                        <li class="scroll active"><a href="about.php">About</a></li>
                        <li class="scroll "><a href="login.php">Login</a></li> 
                        <li class="scroll"><a href="signup.php">Signup</a></li>     
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->
   

    <section id="about">
        <div class="container">

            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">About Us</h2>
                <!-- <p class="text-center wow fadeInDown">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p> -->
            </div>

            <div class="row" style="font-size:16px;">
                <div class="col-sm-6 wow fadeInRight">
                    <h3 class="column-title">Who we are</h3>

                    <p>We are a start up company based out of Philadelphia, City of brotherly love.
                    We help you to communucate easily with people you may need to complete your day. 
                    Communication can be effective and productive.
                    We combine technology with services to provide a compelling platform for our users.</p>
 
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="nostyle">
                                <!-- <li><i class="fa fa-check-square"></i> We help you to communicate easily with a variety of people you may interact on a daily life.</li> -->
                                <li><i class="fa fa-check-square"></i> Our vision is to be your valuable companion at your fingertips wherever you go.</li>
                            </ul>
                        </div> 
                         
                    </div>                     
                </div>

                <div class="col-sm-6 wow fadeInRight">
                    <h3 class="column-title">What we do</h3>
                    <p>Cossist is the primary company you chat with on any queries you may have.
                    Although we don’t claim to to know all the answers , at the least we can guide you in the right direction with a human touch.
                    In addition we provide the same platform to your neighbourhood businesses to connect with you. It is that simple.
                    We built our product to have maximum flexibility between the user and the receiver of your  chat.
                    We take steps to make sure all the messages are answers in a timely manner.</p>
 
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="nostyle">
                                <li><i class="fa fa-check-square"></i> To make ourselves better , we need your comments and feedback.</li>
                                <li><i class="fa fa-check-square"></i> Please use our Feedback section to talk to us or even better download the app and start chating  with us.</li>
                            </ul>
                        </div> 
                         
                    </div>                     
                </div>
        </div>
    </section><!--/#about-->

      
    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2015 Your Company
                </div>
                <div class="col-sm-6">
                    <ul class="social-icons">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        <li><a href="#"><i class="fa fa-behance"></i></a></li>
                        <li><a href="#"><i class="fa fa-flickr"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-github"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/mousescroll.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/jquery.inview.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/main.js"></script>
</body>
</html>