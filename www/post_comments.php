<?php

    require_once "comman.php";

    if(isset($_REQUEST['c_id'])){

		$c_id = $_REQUEST['c_id'];

		$commentdata = getRow('tbl_comments',null,"comments_id = '".$_REQUEST['c_id']."'");

		extract($commentdata);

        if($rating<=3)
            $smile = 'dis0.png';
        else if($rating>3 && $rating<=7)
            $smile = 'smily2.png';
        else
            $smile = 'smily3.png';       
    }
?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title>Airbing : Comment Detail</title>

<meta property="og:title" content="Airbing : Comment Detail"/>
<meta property="og:image" content="http://www.airbing.com/images/<?php echo $smile; ?>"/>
<meta property="og:site_name" content="www.airbing.com"/>
<meta property="og:description" content="<?php echo $airline."&nbsp;:&nbsp;".wordwrap($comment, 50, '<br />', true); ?>"/>
<meta property="og:url" content="http://www.airbing.com/comment_detail.php?c_id=<?php echo $c_id; ?>"/>

<meta name="twitter:title" content="Airbing : Comment Detail" />
<meta name="twitter:description" content="<?php echo $airline."&nbsp;:&nbsp;".substr($comment,0,120); ?>" />
<meta name="twitter:url" content="http://www.airbing.com/comment_detail.php?c_id=<?php echo $c_id; ?>" />
<meta name="twitter:image" content="http://www.airbing.com/images/<?php echo $smile; ?>" />

	<script type="text/javascript">
	   window.location = "http://www.airbing.com/comment_detail.php?c_id=<?php echo $c_id; ?>";
	</script>
</head>

<body>
</body>
</html>